## [3.8.14] - 2025-02-07
### Fixed
- Dont allow Belgium VAT numbers to have less/more than 9 characters after prefix BE0|BE1

## [3.8.13] - 2025-02-06
### Fixed
- Allow Belgium VAT numbers starting with BE1
- Code refactor to meet phpcs, phpstan rules

## [3.8.12] - 2025-01-09
### Fixed
- hide correction button for non pro users

## [3.8.11] - 2025-01-07
### Fixed
- wpml user language saving

## [3.8.10] - 2024-12-12
### Fixed
- division by 0 fatal error when saving document with 0 quantity

## [3.8.9] - 2024-12-11
### Fixed
- allowed corrections to have 0 item quantity

## [3.8.8] - 2024-10-07
### Fixed
- moved country to separate if statement in templates

## [3.8.7] - 2024-06-25
### Fixed
- double link for the document

## [3.8.6] - 2024-05-29
### Fixed
- manual correction

## [3.8.5] - 2024-04-18
### Fixed
- sequential order number

## [3.8.4] - 2024-04-03
### Fixed
- sequential order number

## [3.8.3] - 2024-03-28
### Fixed
- marketing

## [3.8.2] - 2024-03-26
### Fixed
- marketing
- woocommerce sequential order number

## [3.8.1] - 2024-03-20
### Fixed
- marketing

## [3.8.0] - 2024-03-19
### Fixed
- PHP notices
- Total amount update for when product was added manualy
- Order meta fatal error
- Calculating prices from net to gross
- Delete invoice relation
- SKU
- Loading vat number for new order
- Translations

[3.7.36] - 2023-12-19
### Fixed
- added missed assets_url property

## [3.7.35] - 2023-11-22
### Fixed
- currency insert button

## [3.7.34] - 2023-11-14
### Fixed
- critical error on template tab

## [3.7.33] - 2023-11-14
### Fixed
- support links

## [3.7.32] - 2023-11-13
### Fixed
- support links

## [3.7.31] - 2023-10-24
### Fixed
- support links

## [3.7.30] - 2023-10-24
### Fixed
- invoice stylesheet

## [3.7.29] - 2023-10-20
### Fixed
- invoice templates stylesheet

## [3.7.28] - 2023-09-24
### Fixed
- variation SKU number on invoice

## [3.7.27] - 2023-08-28
### Fixed
- Invoice products duplicates on invoice update
## [3.7.26] - 2023-08-24
### Fixed
- NIP validator

## [3.7.25] - 2023-08-21
### Fixed
- Invoice calculation
## [3.7.24] - 2023-08-10
### Fixed
- Remove woocommerce product selector if woocommerce is deactivated
## [3.7.23] - 2023-06-21
### Fixed
- Year prefix translation from french language
## [3.7.22] - 2023-06-21
### Fixed
- rounding total price
- logo on invoice resize
## [3.7.21] - 2023-06-20
### Fixed
- settings container
## [3.7.20] - 2023-06-20
### Added
- library update
## [3.7.19] - 2023-05-18
### Fixed
- empty index error when saving customer vat number
- inserting new tax rates

## [3.7.18] - 2023-04-25
### Fixed
- invoice default font size

## [3.7.17] - 2023-04-24
### Fixed
- minor fixes

## [3.7.17] - 2023-04-24
### Fixed
- minor fixes

## [3.7.16] - 2023-04-20
### Fixed
- free orders settings

## [3.7.15] - 2023-03-06
### Fixed
- role permissions
- fixed netto price calculation on invoice edit page

## [3.7.14] - 2023-03-06
### Fixed
- nip validation
- fixed netto price calculation on invoice edit page

## [3.7.13] - 2023-02-01
### Fixed
- url to css assets
- payment method template
## [3.7.12] - 2023-01-30
### Fixed
- position on invoice calculation 
## [3.7.11] - 2023-01-25
### Fixed
- js error when discount is turned off 
## [3.7.10] - 2023-01-25
### Fixed
- position on invoice calculation 
## [3.7.9] - 2023-01-20
### Fixed
- hide issue correction button
## [3.7.8] - 2022-11-30
### Fixed
- string to float conversion
## [3.7.7] - 2022-11-28
### Fixed
- order numbering for HPOS

## [3.7.6] - 2022-11-23
### Added
- support for WooCommerce high performace order storage 
## [3.7.5] - 2022-11-15
### Fixed
- templates stylesheets
## [3.7.4] - 2022-11-14
### Fixed
- disable templates options if advanced templates are not installed
## [3.7.3] - 2022-10-25
### Fixed
- fixed displaying corrections in order metabox
### Added
- added HRK currency
## [3.7.2] - 2022-10-25
### Fixed
- fixed fatal error for replace_post_messages_filter
### Added
- added HKG currency

## [3.7.1] - 2022-09-08
### Fixed
- fixed fatal error for options_box_callback

### Fixed
- fixed edit page for proforma invoice
- fixed VAT rate when net value and vat amount are not equal to 0
- refactor javascript for invoice edit page
### Added
- added support for Flexible Quantity plugin
- added validation of VAT number when checkbox I want invoice is enabled
- added column with link to invoice on order list page
- added a column with links to generate and send invoices on the order list page
- added comments for order with email shipping status
- moved fields from the Dates metabox to the Publish metabox in invoice editing

## [3.6.3] - 2022-07-14
- template addons

## [3.6.2] - 2022-06-29
- fixed utf-8 encoding
- 
## [3.6.1] - 2022-05-19
### Fixed
- fixed order link setting

## [3.6.0] - 2022-05-12
### Fixed
- polylang support
### Added
- select field for exchange
- invoice url

## [3.5.7] - 2022-05-04
### Fixed
- order completed date
### Added
- exchange options


## [3.5.6] - 2022-04-28
### Fixed
- fatal error when WooCommerce is not enabled

## [3.5.5] - 2022-04-11
### Fixed
- remove reverse charge for non eu countries

## [3.5.4] - 2022-04-07
### Fixed
- state

## [3.5.3] - 2022-03-24
### Fixed
- search customer
### Added
- state field

## [3.5.0] - 2022-01-20
### Fixed
- total & due prices
- fatal in decorator
- translation file
- select2
### Added
- fir integration

## [3.4.7] - 2022-01-12
### Fixed
- hide vat columns
- filter documents
- email HTML

## [3.4.6] - 2021-12-28
### Fixed
- fixed translation

## [3.4.5] - 2021-12-22
### Fixed
- fixed tipTip
- fixed image input field

## [3.4.4] - 2021-12-22
### Fixed
- fixed security issues
- fixed template saving
- fixed VAT number validation
- Drop support for jQuery Datepicker
- Drop template header

### Added
- added new post meta for document
- 
## [3.4.3] - 2021-10-06
### Fixed
- currency locales

## [3.4.2] - 2021-10-06
### Fixed 
- admin css
- document number
- woocommerce sequential order number

## [3.4.1] - 2021-09-08
### Fixed document number

## [3.4.0] - 2021-09-02
### Added
- added fi/core/document/date/format filter
- added fi/core/document/date/issue/format filter
- added fi/core/document/date/payment/format filter
- added fi/core/document/date/paid/format filter
- added fi/core/document/date/sale/format filter
- added fi/core/numbering/prefix/space filter
- added fi/core/is_invoice_ask filter
- added fi/core/is_zero_invoice_ask filter

### Fixed
- fixed cod payment
- fixed invoice posts messages
- fixed invoice columns
- remove ad box

## [3.3.3] - 2021-08-19
### Fixed
- fixed enqueue media
- fixed filling of customer data on the invoice edit page
- fixed saving discount on invoice edit page
- fixed email translation

## [3.3.2] - 2021-08-04
### Fixed
- fixed composer

## [3.3.1] - 2021-07-28
### Fixed
- fixed customer NIP if empty

## [3.3.0] - 2021-07-28
### Fixed
- fixed tax rate index for product
- fixed tax rates
- fixed changelog
- fixed quantity type to float
- fixed country label
- fixed signature user hook
### Added
- added support for OSS
- added function to get document download url
- added fi/core/register_post_type/can_export filter

## [3.2.2] - 2021-07-12
### Fixed
- fixed tax rate for MOSS

### Added
- added debug meta box

## [3.2.1] - 2021-06-24
### Added
- hook docs

### Fixed
- fixed issue for downloaded PDF
- fixed net price

## [3.2.0] - 2021-06-15
### Added
- document templates

## [3.0.0] - 2021-05-04
### Added
- fi/core/woocommerce/document/item/skip
- fi/core/woocommerce/document/item/title
- fi/core/woocommerce/document/item/show_meta
- classes to create items for other integrations
- select field for multiple statuses

### Fixed
- vat rates
- issuing a document when the product does not exist
### Remove
- woocommerce_add_variant_info settings 

## [2.0.0] - 2021-05-04
### Added
- fi/core/email/after/send hook
- fi/core/email/before/send hook
### Fixed
- integration

## [1.3.5] - 2021-04-21
### Fixed
- fixed report

## [1.3.4] - 2021-04-20
### Fixed
- suppress filters

## [1.3.3] - 2021-04-14
### Fixed
- css

## [1.3.2] - 2021-04-13
### Fixed
- js undefined variable

## [1.3.1] - 2021-04-12
### Added
- custom meta fields

## [1.3.0] - 2021-04-09
### Fixed
- disable cache for numbering wp_option to ensure synchronized access
- fixed PDF library
- fixed email template
### Added
- custom meta fields

## [1.2.6] - 2021-03-11
### Fixed
- translations

## [1.2.5] - 2021-03-10
### Fixed
- qty notice
### Added
- fi/core/numbering/formatted_number filter

## [1.2.4] - 2021-02-18
### Fixed
- becon
- select2 translations

## [1.2.3] - 2021-02-11
### Fixed
- translations

## [1.2.2] - 2021-02-11
### Fixed
- fixed pending status
- fixed invoice_ask_field
- fixed vat number field visibility
- added reverse charge for orders without vat

## [1.2.1] - 2021-01-25
### Fixed
- fixed checkout fields
- fixed customer address
- fixed translations

## [1.2.0] - 2021-01-21
### Fixed
- fixed translation
- refactor decorators
- fixed saving meta data
- added select for user country
- fixed settings
- fixed correction reset

## [1.1.1] - 2021-01-04
### Fixed
- fixed translation

## [1.1.0] - 2021-01-04
### Fixed
- fixed numbering

## [1.0.6] - 2020-12-14
### Fixed
- fixed due prices
- fixed order number in pdf
- fixed dates
- fixed document number

## [1.0.5] - 2020-12-07
### Fixed
- fixed template

## [1.0.4] - 2020-11-27
### Fixed
- order number in column
- fixed vat_number for no logged users

## [1.0.3] - 2020-11-24
### Fixed
- order number in column

## [1.0.2] - 2020-11-24
### Fixed
- correction prefix on update

## [1.0.1] - 2020-11-23
### Fixed
- translation

## [1.0.0] - 2020-11-06
### Added
- init
