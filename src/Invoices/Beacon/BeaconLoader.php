<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Beacon;

use WPDesk\Beacon\BeaconPro;
use WPDesk\Library\FlexibleInvoicesCore\LibraryInfo;
use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Class BeaconLoaderAction, Beacon loader.
 *
 * @package WPDesk\Library\FlexibleInvoicesCore\Beacon
 */
class BeaconLoader implements Hookable {

	/**
	 * @var LibraryInfo
	 */
	private $plugin_info;

	/**
	 * @param LibraryInfo $plugin_info
	 */
	public function __construct( LibraryInfo $plugin_info ) {
		$this->plugin_info = $plugin_info;
	}

	/**
	 * Fire hooks.
	 */
	public function hooks() {
		add_action( 'init', [ $this, 'init_beacon' ], 10 );
	}

	/**
	 * Init beacon.
	 */
	public function init_beacon() {
		$beacon_id = '17f6054b-a2fb-4ee7-8bb5-0c3cbad1ef6a';
		$beacon    = new BeaconPro( //@phpstan-ignore-line
			$beacon_id,
			new BeaconShowStrategy(),
			$this->plugin_info->get_plugin_url() . 'vendor_prefixed/wpdesk/wp-helpscout-beacon/assets/'
		);
		$beacon->hooks(); //@phpstan-ignore-line
	}
}
