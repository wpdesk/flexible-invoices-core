<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Documents\Items;

/**
 * Discount item.
 *
 * This type of item can be used as product that stores total of discounted products.
 *
 * @package WPDesk\Library\FlexibleInvoicesCore\Documents\Items
 */
class DiscountItem extends DocumentItem {

	const TYPE = 'discount';
}
