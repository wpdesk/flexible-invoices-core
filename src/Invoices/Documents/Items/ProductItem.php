<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Documents\Items;

class ProductItem extends DocumentItem {

	const TYPE = 'product';
}
