<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Documents\Items;

class ShippingItem extends DocumentItem {

	const TYPE = 'shipping';
}
