<?php

namespace WPDesk\Library\FlexibleInvoicesCore\DocumentsMeta;

/**
 * Null custom meta.
 *
 * @package WPDesk\Library\FlexibleInvoicesCore\Creators
 */
class NullCustomMeta extends DocumentCustomMeta {

	/**
	 * @return void
	 */
	public function save() {
	}
}
