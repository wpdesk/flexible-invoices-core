<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Email;

use WC_Order;
use WPDesk\Library\FlexibleInvoicesAbstracts\Documents\Document;
use WPDesk\Library\FlexibleInvoicesCore\WordPress\PDF;

/**
 * @package WPDesk\Library\FlexibleInvoicesCore\Email
 */
interface DocumentEmail {

	/**
	 * @param WC_Order $order
	 * @param Document  $document
	 * @param PDF       $pdf
	 *
	 * @return void
	 */
	public function should_send_email( WC_Order $order, Document $document, PDF $pdf );
}
