<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Email;

use Exception;
use WC_Order;
use WPDesk\Library\FlexibleInvoicesAbstracts\Documents\Document;
use WPDesk\Library\FlexibleInvoicesCore\Helpers\EmailStatus;
use WPDesk\Library\FlexibleInvoicesCore\Helpers\Invoice;
use WPDesk\Library\FlexibleInvoicesCore\Helpers\WooCommerce;
use WPDesk\Library\FlexibleInvoicesCore\Infrastructure\Request;
use WPDesk\Library\FlexibleInvoicesCore\Integration\DocumentFactory;
use WPDesk\Library\FlexibleInvoicesCore\WooCommerce\OrderNote;
use WPDesk\Library\FlexibleInvoicesCore\WordPress\PDF;
use WPDesk\Library\FlexibleInvoicesCore\WordPress\Translator;
use WPDesk\PluginBuilder\Plugin\Hookable;
use function WC;

/**
 * @package WPDesk\Library\FlexibleInvoicesCore\Email
 */
class EmailIntegration implements Hookable {

	/**
	 * @var DocumentFactory
	 */
	private $document_factory;

	/**
	 * @var PDF
	 */
	private $pdf;

	/**
	 * @var OrderNote
	 */
	private $order_note;

	/**
	 * @param DocumentFactory $document_factory
	 * @param PDF             $pdf
	 */
	public function __construct( DocumentFactory $document_factory, PDF $pdf, OrderNote $order_note ) {
		$this->document_factory = $document_factory;
		$this->pdf              = $pdf;
		$this->order_note       = $order_note;
	}

	/**
	 * Fire hooks.
	 */
	public function hooks() {
		add_action( 'wp_ajax_fi_send_email', [ $this, 'send_document' ] );
	}

	/**
	 * @return void
	 * @internal You should not use this directly from another application
	 */
	public function send_document() {
		$request = new Request();
		$id      = (int) $request->param( 'get.document_id' )->get();
		$nonce   = $request->param( 'get._wpnonce' )->get();

		if ( $id && ( $nonce && wp_verify_nonce( $nonce ) ) ) { //@phpstan-ignore-line
			try {
				$creator  = $this->document_factory->get_document_creator( $id );
				$document = $creator->get_document();
				$client   = $document->get_customer();
				$order_id = $document->get_order_id();
				if ( empty( $client->get_email() ) || ! is_email( $client->get_email() ) ) {
					wp_send_json_error(
						[
							'invoice_number' => '',
							'msg'            => esc_html__( 'Email address is blank or invalid!', 'flexible-invoices-core' ),
						]
					);
				}

				$note = '';
				if ( $order_id ) {
					$order = wc_get_order( $order_id );
					if ( $order ) {
						$send = $this->send_email( $order, $document, 'fi_' . $document->get_type() );
					} else {
						$send = $this->send_manual_email( $document );
					}
					// translators: %s - document name
					$note = sprintf( esc_html__( '%s was send to the customer', 'flexible-invoices-core' ), $creator->get_name() );
					$this->order_note->add_note( $order, $note );
				} else {
					$send = $this->send_manual_email( $document );
				}

				if ( $send ) {
					wp_send_json_success(
						[
							'invoice_number' => $document->get_formatted_number(),
							'msg'            => esc_html__( 'Email was sent!', 'flexible-invoices-core' ),
							'email'          => $client->get_email(),
							'note'           => $note,
						]
					);
				}
			} catch ( Exception $e ) {
				wp_send_json_error(
					[
						'invoice_number' => '',
						'msg'            => $e->getMessage(),
						'email'          => '',
					]
				);
			}
		}

		wp_send_json_error(
			[
				'invoice_number' => '',
				'status'         => false,
				'msg'            => esc_html__( 'Invalid nonce or document ID', 'flexible-invoices-core' ),
				'email'          => '',
			]
		);
	}

	/**
	 * @param WC_Order $order
	 * @param Document $document
	 * @param string   $email_class
	 *
	 * @return bool
	 */
	public function send_email( WC_Order $order, Document $document, string $email_class ): bool {
		Translator::switch_lang( $document->get_user_lang() );
		Translator::set_translate_lang( $document->get_user_lang() );
		$mailer = WC()->mailer();
		$emails = $mailer->get_emails();
		$client = $document->get_customer();
		if ( ! empty( $emails[ $email_class ] ) && ! empty( $client->get_email() ) ) {
			if ( $emails[ $email_class ] instanceof DocumentEmail ) {
				/**
				 * @var DocumentEmail $emails[ $email_class ]
				 */
				$emails[ $email_class ]->should_send_email( $order, $document, $this->pdf );
			}

			return true;
		}

		return false;
	}

	/**
	 * @param Document $document
	 *
	 * @return bool
	 */
	public function send_manual_email( Document $document ): bool {
		Translator::switch_lang( $document->get_user_lang() );
		Translator::set_translate_lang( $document->get_user_lang() );
		$mailer = WC()->mailer();
		$emails = $mailer->get_emails();
		$client = $document->get_customer();
		if ( ! empty( $client->get_email() ) ) {
			$emails['fi_invoice_manual']->should_send_email( $document, $this->pdf ); //@phpstan-ignore-line

			return true;
		}

		return false;
	}
}
