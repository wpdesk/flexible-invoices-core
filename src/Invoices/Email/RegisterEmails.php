<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Email;

use WPDesk\Library\FlexibleInvoicesCore\Integration\DocumentFactory;
use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Register email classes for WooCommerce.
 *
 * @package WPDesk\WooCommerceFakturownia\Email
 */
class RegisterEmails implements Hookable {

	/**
	 * @var DocumentFactory
	 */
	private $document_factory;

	/**
	 * @param DocumentFactory $document_factory
	 */
	public function __construct( DocumentFactory $document_factory ) {
		$this->document_factory = $document_factory;
	}

	/**
	 * Hooks
	 */
	public function hooks() {
		add_filter( 'woocommerce_email_classes', [ $this, 'register_emails' ], 11 );
	}

	/**
	 * Register emails in WooCommerce.
	 *
	 * @param array $emails Emails.
	 *
	 * @return array
	 */
	public function register_emails( array $emails ): array {
		foreach ( $this->document_factory->get_creators() as $creator ) {
			$emails[ 'fi_' . $creator->get_type() ] = $creator->get_email_class();
		}

		$emails['fi_invoice_manual'] = new EmailManualInvoice();

		return $emails;
	}
}
