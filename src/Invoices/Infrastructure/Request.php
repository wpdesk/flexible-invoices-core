<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Infrastructure;

/**
 * Parse requests
 *
 * @package WPDesk\Library\FlexibleInvoicesCore\Infrastructure
 */
class Request {

	/**
	 * @var array
	 */
	private $parameters;

	public function __construct() {
		$this->parameters = [
			'METHOD'  => strtoupper( $_SERVER['REQUEST_METHOD'] ?? 'GET' ), // phpcs:ignore
			'GET'     => $_GET, // phpcs:ignore
			'POST'    => $_POST, // phpcs:ignore WordPress.Security.NonceVerification.Missing
			'FILES'   => $_FILES, // phpcs:ignore WordPress.Security.NonceVerification.Missing
			'COOKIE'  => $_COOKIE,
			'SERVER'  => $_SERVER,
			'SESSION' => $_SESSION ?? [],
			'INPUT'   => file_get_contents( 'php://input' ),
		];
	}

	/**
	 * @param $param
	 *
	 * @return DataType
	 */
	public function param( $param ): DataType {
		return $this->get_param_from_array( $param );
	}

	/**
	 * @param $param
	 *
	 * @return bool
	 */
	public function param_exists( $param ): bool {
		return $this->get_param_from_array( $param )->has();
	}

	/**
	 * @param string $param
	 *
	 * @return DataType
	 */
	private function get_param_from_array( string $param ): DataType {
		$keys       = explode( '.', $param );
		$parameters = $this->parameters;
		foreach ( $keys as $key ) {
			$parameters = array_change_key_case( $parameters, CASE_UPPER );
			$key        = strtoupper( $key );
			if ( isset( $parameters[ $key ] ) ) {
				$parameters = $parameters[ $key ];
			} else {
				$parameters = null;
				break;
			}
		}

		return new DataType( $parameters );
	}
}
