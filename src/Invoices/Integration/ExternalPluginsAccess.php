<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Integration;

use Psr\Log\LoggerInterface;
use WPDesk\Library\FlexibleInvoicesCore\LibraryInfo;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Settings;
use WPDesk\Library\FlexibleInvoicesCore\SettingsStrategy\SettingsStrategy;
use WPDesk\Library\FlexibleInvoicesCore\WordPress\PDF;
use WPDesk\View\Renderer\Renderer;

/**
 * Class that grants access to some internal classes and info about Flexible Invoice to external plugins.
 *
 * @package WPDesk\ShopMagic\Integration
 */
class ExternalPluginsAccess {

	/**
	 * @var string
	 */
	private $version;

	/**
	 * @var DocumentFactory
	 */
	private $document_factory;

	/**
	 * @var SaveDocument
	 */
	private $document_saver;

	/**
	 * @var SettingsStrategy
	 */
	private $settings_strategy;

	/**
	 * @var Settings
	 */
	private $settings;

	/**
	 * @var LibraryInfo
	 */
	private $library_info;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @var Renderer
	 */
	private $renderer;

	/**
	 * @var PDF
	 */
	private $pdf;

	/**
	 * @param string           $version
	 * @param DocumentFactory  $document_factory
	 * @param SaveDocument     $document_saver
	 * @param SettingsStrategy $settings_strategy
	 * @param Settings         $settings
	 * @param LibraryInfo      $library_info
	 * @param LoggerInterface  $logger
	 * @param Renderer         $renderer
	 * @param PDF              $pdf
	 */
	public function __construct(
		string $version,
		DocumentFactory $document_factory,
		SaveDocument $document_saver,
		SettingsStrategy $settings_strategy,
		Settings $settings,
		LibraryInfo $library_info,
		LoggerInterface $logger,
		Renderer $renderer,
		PDF $pdf
	) {
		$this->version           = $version;
		$this->document_factory  = $document_factory;
		$this->document_saver    = $document_saver;
		$this->settings_strategy = $settings_strategy;
		$this->settings          = $settings;
		$this->library_info      = $library_info;
		$this->logger            = $logger;
		$this->renderer          = $renderer;
		$this->pdf               = $pdf;
	}

	/**
	 * @return string
	 */
	public function get_version(): string {
		return $this->version;
	}

	/**
	 * @return DocumentFactory
	 */
	public function get_document_factory(): DocumentFactory {
		return $this->document_factory;
	}

	/**
	 * @return SaveDocument
	 */
	public function get_document_saver(): SaveDocument {
		return $this->document_saver;
	}

	/**
	 * @return SettingsStrategy
	 */
	public function get_settings_strategy(): SettingsStrategy {
		return $this->settings_strategy;
	}

	/**
	 * @return Settings
	 */
	public function get_settings(): Settings {
		return $this->settings;
	}

	/**
	 * @return LibraryInfo
	 */
	public function get_library_info(): LibraryInfo {
		return $this->library_info;
	}

	/**
	 * @return LoggerInterface
	 */
	public function get_logger(): LoggerInterface {
		return $this->logger;
	}

	/**
	 * @return Renderer
	 */
	public function get_renderer(): Renderer {
		return $this->renderer;
	}

	/**
	 * @return PDF
	 */
	public function get_pdf(): PDF {
		return $this->pdf;
	}
}
