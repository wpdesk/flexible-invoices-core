<?php

namespace WPDesk\Library\FlexibleInvoicesCore\PDF;

use WPDesk\Library\FlexibleInvoicesAbstracts\Documents\Document;

interface PdfPrinter {

	/**
	 * @return string
	 */
	public function get_as_string( Document $document ): string;
}
