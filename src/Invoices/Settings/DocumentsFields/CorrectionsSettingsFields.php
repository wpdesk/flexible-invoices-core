<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Settings\DocumentsFields;

use WPDesk\Forms\Field\TextAreaField;
use WPDesk\Library\FlexibleInvoicesCore\Helpers\Plugin;
use WPDesk\Library\FlexibleInvoicesCore\Helpers\WooCommerce;
use WPDesk\Library\FlexibleInvoicesCore\InvoicesIntegration;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\DisableFieldProAdapter;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\FICheckboxField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\SubEndField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\SubStartField;
use WPDesk\Forms\Field\Header;
use WPDesk\Forms\Field\InputTextField;
use WPDesk\Forms\Field\SelectField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\WPMLFieldDecorator;

/**
 * Correction Document Settings Sub Page.
 *
 * @package WPDesk\Library\FlexibleInvoicesCore\Settings\DocumentsFields
 */
final class CorrectionsSettingsFields implements DocumentsFieldsInterface {

	/**
	 * @return string
	 */
	private function get_doc_link() {
		if ( InvoicesIntegration::is_super() ) {
			$docs_link = 'https://docs.flexibleinvoices.com/article/802-manual-issuing-corrections?utm_source=flexible-invoices-settings&utm_medium=link&utm_campaign=settings-docs-link';
			if ( get_locale() === 'pl_PL' ) {
				$docs_link = 'https://www.wpdesk.pl/docs/faktury-korygujace-woocommerce/?utm_source=flexible-invoices-settings&utm_medium=link&utm_campaign=settings-docs-link';
			}

			// translators: %1$s: link, %2$s: strong, %3$s: /strong
			return sprintf( '%2$s%1$s%3$s', sprintf( esc_html__( 'Read more in the %1$splugin documentation &rarr;%2$s', 'flexible-invoices-core' ), '<a href="' . $docs_link . '" target="_blank" style="color: #4BB04E; font-weight: 700;">', '</a>' ), '<strong>', '</strong>' );
		} else {
			return sprintf(
				'<a href="%1$s&utm_content=correction" target="_blank" style="color: #8f0350; font-weight: 700;">%2$s</a>',
				Plugin::upgrade_to_pro_url(),
				esc_html__( 'Upgrade to PRO and enable options below →', 'flexible-invoices-core' )
			);
		}
	}


	private function get_beacon_translations(): string {
		return esc_html__( 'Correction Settings', 'flexible-invoices-core' );
	}

	/**
	 * @return array|\WPDesk\Forms\Field[]
	 */
	public function get_fields() {

		$invoice_beacon = $this->get_beacon_translations();

		return [
			( new SubStartField() )
				->set_label( esc_html__( 'Correction', 'flexible-invoices-core' ) )
				->set_name( 'correction' ),
			( new Header() )
				->set_label( esc_html__( 'Correction Settings', 'flexible-invoices-core' ) )
				->set_description( $this->get_doc_link() ),

			( new DisableFieldProAdapter(
				'enable_corrections',
				( new FICheckboxField() )
					->set_name( '' )
					->set_label( esc_html__( 'Automatic Corrections', 'flexible-invoices-core' ) )
					->set_sublabel( esc_html__( 'Enable automatic corrections generation for order refunds.', 'flexible-invoices-core' ) )
					->add_class( 'hs-beacon-search' )
					->set_attribute( 'data-beacon_search', $invoice_beacon )
			) )->get_field(),

			( new DisableFieldProAdapter(
				'correction_number_reset_type',
				( new SelectField() )
					->set_name( '' )
					->set_label( esc_html__( 'Number Reset', 'flexible-invoices-core' ) )
					->set_description( esc_html__( 'Select when to reset the correction number to 1.', 'flexible-invoices-core' ) )
					->set_options(
						[
							'year'  => esc_html__( 'Yearly', 'flexible-invoices-core' ),
							'month' => esc_html__( 'Monthly', 'flexible-invoices-core' ),
							'none'  => esc_html__( 'None', 'flexible-invoices-core' ),
						]
					)
					->add_class( 'hs-beacon-search' )
					->set_attribute( 'data-beacon_search', $invoice_beacon )
			) )->get_field(),

			( new DisableFieldProAdapter(
				'correction_start_number',
				( new InputTextField() )
					->set_name( '' )
					->set_label( esc_html__( 'Next Number', 'flexible-invoices-core' ) )
					->set_description( esc_html__( 'Enter the next correction number. The default value is 1 and changes every time a correction is issued. Existing corrections won\'t be changed.', 'flexible-invoices-core' ) )
					->add_class( 'regular-text edit_disabled_field hs-beacon-search' )
					->set_attribute( 'type', 'number' )
					->set_attribute( 'data-beacon_search', $invoice_beacon )
			) )->get_field(),

			( new DisableFieldProAdapter(
				'correction_number_prefix',
				( new WPMLFieldDecorator(
					( new InputTextField() )
						->set_name( '' )
						->set_label( esc_html__( 'Prefix', 'flexible-invoices-core' ) )
						->set_default_value( '1' )
						->set_default_value( esc_html__( 'Corrected invoice', 'flexible-invoices-core' ) )
						->add_class( 'regular-text hs-beacon-search' )
						->set_description( wp_kses( __( 'For prefixes use the following short tags: <code>{DD}</code> for day, <code>{MM}</code> for month, <code>{YYYY}</code> for year.', 'flexible-invoices-core' ), [ 'code' => [] ] ) )
						->set_attribute( 'data-beacon_search', $invoice_beacon )
				) )->get_field()
			) )->get_field(),

			( new DisableFieldProAdapter(
				'correction_number_suffix',
				( new WPMLFieldDecorator(
					( new InputTextField() )
						->set_name( '' )
						->set_label( esc_html__( 'Suffix', 'flexible-invoices-core' ) )
						->set_default_value( esc_html__( '/{MM}/{YYYY}', 'flexible-invoices-core' ) )
						->add_class( 'regular-text hs-beacon-search' )
						->set_description( wp_kses( __( 'For suffixes use the following short tags: <code>{DD}</code> for day, <code>{MM}</code> for month, <code>{YYYY}</code> for year.', 'flexible-invoices-core' ), [ 'code' => [] ] ) )
						->set_attribute( 'data-beacon_search', $invoice_beacon )
				) )->get_field()
			) )->get_field(),

			( new DisableFieldProAdapter(
				'correction_default_due_time',
				( new InputTextField() )
					->set_name( '' )
					->set_label( esc_html__( 'Default Due Time', 'flexible-invoices-core' ) )
					->set_default_value( '0' )
					->set_attribute( 'type', 'number' )
					->add_class( 'regular-text hs-beacon-search' )
					->set_attribute( 'data-beacon_search', $invoice_beacon )
			) )->get_field(),

			( new DisableFieldProAdapter(
				'correction_notes',
				( new WPMLFieldDecorator(
					( new TextAreaField() )
						->set_name( '' )
						->set_label( esc_html__( 'Reason', 'flexible-invoices-core' ) )
						->set_default_value( esc_attr__( 'Refund', 'flexible-invoices-core' ) )
						->add_class( 'large-text hs-beacon-search' )
						->set_attribute( 'data-beacon_search', $invoice_beacon )
				) )->get_field()
			) )->get_field(),

			( new SubEndField() )
				->set_label( '' ),
		];
	}

	/**
	 * @inheritDoc
	 */
	public static function get_tab_slug() {
		return 'corrections';
	}

	/**
	 * @inheritDoc
	 */
	public function get_tab_name() {
		return esc_html__( 'Corrections', 'flexible-invoices-core' );
	}
}
