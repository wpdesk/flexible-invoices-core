<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Settings\DocumentsFields;

use WPDesk\Forms\Field\WooSelect;
use WPDesk\Library\FlexibleInvoicesCore\Helpers\WooCommerce;
use WPDesk\Library\FlexibleInvoicesCore\InvoicesIntegration;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\DisableFieldProAdapter;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\FICheckboxField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\SubEndField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\SubStartField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\WPMLFieldDecorator;
use WPDesk\Library\FlexibleInvoicesCore\SettingsStrategy\SettingsStrategy;
use WPDesk\Forms\Field\Header;
use WPDesk\Forms\Field\InputTextField;
use WPDesk\Forms\Field\SelectField;
use WPDesk\Forms\Field\TextAreaField;

/**
 * Invoice Document Settings Sub Page.
 *
 * @package WPDesk\Library\FlexibleInvoicesCore\Settings\DocumentsFields
 */
final class InvoicesSettingsFields implements DocumentsFieldsInterface {

	/**
	 * @var SettingsStrategy
	 */
	private $strategy;

	/**
	 * @param SettingsStrategy $strategy
	 */
	public function __construct( SettingsStrategy $strategy ) {
		$this->strategy = $strategy;
	}

	/**
	 * @return string
	 */
	private function get_doc_link(): string {
		$docs_link = 'https://docs.flexibleinvoices.com/article/794-invoice-settings?utm_source=flexible-invoices-settings&utm_medium=link&utm_campaign=flexible-invoices-docs-link';
		if ( get_locale() === 'pl_PL' ) {
			$docs_link = 'https://www.wpdesk.pl/docs/faktury-woocommerce-docs/?utm_source=wp-admin-plugins&utm_medium=quick-link&utm_campaign=flexible-invoices-docs-link#faktury';
		}

		/* translators: %1$s - open tag, %2$s - close tag */
		return sprintf( '%2$s%1$s%3$s', sprintf( esc_html__( 'Read more in the %1$splugin documentation &rarr;%2$s', 'flexible-invoices-core' ), '<a href="' . $docs_link . '" target="_blank" style="color: #4BB04E; font-weight: 700;">', '</a>' ), '<strong>', '</strong>' );
	}

	private function get_beacon_translations(): string {
		return 'Invoice Settings';
	}

	/**
	 * @return array|\WPDesk\Forms\Field[]
	 */
	public function get_fields() {
		$invoice_beacon = $this->get_beacon_translations();
		$fields         = [
			( new SubStartField() )
				->set_label( esc_html__( 'Invoice', 'flexible-invoices-core' ) )
				->set_name( 'invoice' ),
			( new Header() )
				->set_label( esc_html__( 'Invoice Settings', 'flexible-invoices-core' ) )
				->set_description( '<strong>' . $this->get_doc_link() . '</strong>' ),
			( new DisableFieldProAdapter(
				'invoice_auto_paid_status',
				( new FICheckboxField() )
					->set_name( 'invoice-status' )
					->set_label( esc_html__( 'Invoice Status', 'flexible-invoices-core' ) )
					->set_sublabel( esc_html__( 'Change invoice status to paid after an order is completed', 'flexible-invoices-core' ) )
					->add_class( ' hs-beacon-search' )
					->set_attribute( 'data-beacon_search', $invoice_beacon ),
				true
			) )->get_field(),

			( new DisableFieldProAdapter(
				'invoice_auto_create_status',
				( new WooSelect() )
					->set_name( 'automatic-invoices' )
					->set_label( esc_html__( 'Issue invoices automatically', 'flexible-invoices-core' ) )
					->set_description( esc_html__( 'If you want to issue invoices automatically, select order status. When the order status is changed to selected an invoice will be generated and a link to a PDF file will be attached to an e-mail.', 'flexible-invoices-core' ) )
					->set_options(
						$this->strategy->get_order_statuses()
					)
					->set_multiple()
					->add_class( ' hs-beacon-search select2' )
					->set_attribute( 'data-beacon_search', $invoice_beacon ),
				true
			) )->get_field(),
			( new InputTextField() )
				->set_name( 'invoice_start_number' )
				->set_label( esc_html__( 'Next Number', 'flexible-invoices-core' ) )
				->set_default_value( '1' )
				->set_attribute( 'type', 'number' )
				->add_class( 'hs-beacon-search edit_disabled_field' )
				->set_attribute( 'disabled', 'disabled' )
				->set_description( esc_html__( 'Enter the next invoice number. The default value is 1 and changes every time an invoice is issued. Existing invoices won\'t be changed.', 'flexible-invoices-core' ) )
				->set_attribute( 'data-beacon_search', $invoice_beacon ),
			( new WPMLFieldDecorator(
				( new InputTextField() )
					->set_name( 'invoice_number_prefix' )
					->set_default_value( esc_html__( 'Invoice ', 'flexible-invoices-core' ) )
					->set_label( esc_html__( 'Prefix', 'flexible-invoices-core' ) )
					->add_class( 'hs-beacon-search' )
					->set_description( wp_kses( __( 'For prefixes use the following short tags: <code>{DD}</code> for day, <code>{MM}</code> for month, <code>{YYYY}</code> for year.', 'flexible-invoices-core' ), [ 'code' => [] ] ) )
					->set_attribute( 'data-beacon_search', $invoice_beacon )
			) )->get_field(),
			( new WPMLFieldDecorator(
				( new InputTextField() )
					->set_name( 'invoice_number_suffix' )
					->set_default_value( '/{MM}/{YYYY}' )
					->set_label( esc_html__( 'Suffix', 'flexible-invoices-core' ) )
					->add_class( 'hs-beacon-search' )
					->set_description( wp_kses( __( 'For suffixes use the following short tags: <code>{DD}</code> for day, <code>{MM}</code> for month, <code>{YYYY}</code> for year.', 'flexible-invoices-core' ), [ 'code' => [] ] ) )
					->set_attribute( 'data-beacon_search', $invoice_beacon )
			) )->get_field(),
			( new SelectField() )
				->set_name( 'invoice_number_reset_type' )
				->set_label( esc_html__( 'Number Reset', 'flexible-invoices-core' ) )
				->add_class( 'hs-beacon-search' )
				->set_description( esc_html__( 'Select when to reset the invoice number to 1.', 'flexible-invoices-core' ) )
				->set_options(
					[
						'year'  => esc_html__( 'Yearly', 'flexible-invoices-core' ),
						'month' => esc_html__( 'Monthly', 'flexible-invoices-core' ),
						'none'  => esc_html__( 'None', 'flexible-invoices-core' ),
					]
				)
				->set_attribute( 'data-beacon_search', $invoice_beacon ),
			( new InputTextField() )
				->set_name( 'invoice_default_due_time' )
				->set_default_value( '0' )
				->set_attribute( 'type', 'number' )
				->set_label( esc_html__( 'Default Due Time', 'flexible-invoices-core' ) )
				->add_class( 'hs-beacon-search' )
				->set_attribute( 'data-beacon_search', $invoice_beacon ),
			( new WPMLFieldDecorator(
				( new InputTextField() )
					->set_name( 'invoice_date_of_sale_label' )
					->set_label( esc_html__( 'Label for Date of Sale', 'flexible-invoices-core' ) )
					->add_class( 'hs-beacon-search' )
					->set_default_value( esc_html__( 'Date of sale', 'flexible-invoices-core' ) )
					->set_description( esc_html__( 'Enter the label for "date of sale" visible on the PDF invoice, i.e. "date of delivery". It will be used on all new and edited invoices.', 'flexible-invoices-core' ) )
					->set_attribute( 'data-beacon_search', $invoice_beacon )
			) )->get_field(),
			( new WPMLFieldDecorator(
				( new TextAreaField() )
					->set_name( 'invoice_notes' )
					->set_label( esc_html__( 'Notes', 'flexible-invoices-core' ) )
					->add_class( 'large-text wide-input hs-beacon-search' )
					->set_attribute( 'data-beacon_search', $invoice_beacon )
			) )->get_field(),
			( new SubEndField() )
				->set_label( '' ),
		];

		return $fields;
	}

	/**
	 * @return string
	 */
	public static function get_tab_slug() {
		return 'invoices';
	}

	/**
	 * @return string
	 */
	public function get_tab_name() {
		return esc_html__( 'Invoices', 'flexible-invoices-core' );
	}
}
