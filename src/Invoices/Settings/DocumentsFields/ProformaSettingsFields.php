<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Settings\DocumentsFields;

use WPDesk\Forms\Field\WooSelect;
use WPDesk\Library\FlexibleInvoicesCore\Helpers\Plugin;
use WPDesk\Library\FlexibleInvoicesCore\Helpers\WooCommerce;
use WPDesk\Library\FlexibleInvoicesCore\InvoicesIntegration;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\DisableFieldProAdapter;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\SubEndField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\SubStartField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\WPMLFieldDecorator;
use WPDesk\Library\FlexibleInvoicesCore\SettingsStrategy\SettingsStrategy;
use WPDesk\Forms\Field\Header;
use WPDesk\Forms\Field\InputTextField;
use WPDesk\Forms\Field\SelectField;
use WPDesk\Forms\Field\TextAreaField;

/**
 * Invoice Proforma Document Settings Sub Page.
 *
 * @package WPDesk\Library\FlexibleInvoicesCore\Settings\DocumentsFields
 */
final class ProformaSettingsFields implements DocumentsFieldsInterface {

	/**
	 * @var SettingsStrategy
	 */
	private $strategy;

	/**
	 * @param SettingsStrategy $strategy
	 */
	public function __construct( SettingsStrategy $strategy ) {
		$this->strategy = $strategy;
	}

	/**
	 * @return string
	 */
	private function get_doc_link(): string {
		if ( InvoicesIntegration::is_super() ) {
			$docs_link = 'https://docs.flexibleinvoices.com/article/796-proforma-settings?utm_source=flexible-invoices-settings&utm_medium=link&utm_campaign=flexible-invoices-docs-link';
			if ( get_locale() === 'pl_PL' ) {
				$docs_link = 'https://www.wpdesk.pl/docs/faktury-woocommerce-docs/?utm_source=wp-admin-plugins&utm_medium=quick-link&utm_campaign=flexible-invoices-docs-link#proformy';
			}

			/* translators: %1$s: link, %2$s: strong, %3$s: /strong */
			return sprintf( '%2$s%1$s%3$s', sprintf( esc_html__( 'Read more in the %1$splugin documentation &rarr;%2$s', 'flexible-invoices-core' ), '<a href="' . $docs_link . '" target="_blank" style="color: #4BB04E; font-weight: 700;">', '</a>' ), '<strong>', '</strong>' );
		} else {
			return sprintf(
				'<a href="%1$s&utm_content=proforma" style="color: #8f0350; font-weight: 700;" target="_blank">%2$s</a>',
				Plugin::upgrade_to_pro_url(),
				esc_html__( 'Upgrade to PRO and enable options below →', 'flexible-invoices-core' )
			);
		}
	}

	/**
	 * @return array
	 */
	public function get_order_statuses(): array {
		$statuses = $this->strategy->get_order_statuses();
		unset( $statuses['completed'] );

		return $statuses;
	}

	private function get_beacon_translations(): string {
		return 'Proforma Settings';
	}

	/**
	 * @return array|\WPDesk\Forms\Field[]
	 */
	public function get_fields(): array {
		$invoice_beacon = $this->get_beacon_translations();

		return [
			( new SubStartField() )
				->set_label( esc_html__( 'Proforma', 'flexible-invoices-core' ) )
				->set_name( 'proforma' ),
			( new Header() )
				->set_label( esc_html__( 'Proforma Invoice Settings', 'flexible-invoices-core' ) )
				->set_description( $this->get_doc_link() ),
			( new DisableFieldProAdapter(
				'proforma_auto_create_status',
				( new WooSelect() )
					->set_name( '' )
					->set_label( esc_html__( 'Issue proforma invoices automatically', 'flexible-invoices-core' ) )
					->set_description( esc_html__( 'If you want to issue proforma invoices automatically, select order status. When the order status is changed to selected, a proforma invoice will be generated and a link to a PDF file will be attached to an e-mail.', 'flexible-invoices-core' ) )
					->set_options(
						$this->get_order_statuses()
					)
					->add_class( 'hs-beacon-search select2' )
					->set_multiple()
					->set_attribute( 'data-beacon_search', $invoice_beacon )
			) )->get_field(),

			( new DisableFieldProAdapter(
				'proforma_start_number',
				( new InputTextField() )
					->set_name( '' )
					->set_label( esc_html__( 'Next Number', 'flexible-invoices-core' ) )
					->set_default_value( '1' )
					->set_attribute( 'type', 'number' )
					->add_class( 'edit_disabled_field hs-beacon-search' )
					->set_description( esc_html__( 'Enter the next invoice number. The default value is 1 and changes every time an invoice is issued. Existing invoices won\'t be changed.', 'flexible-invoices-core' ) )
					->set_attribute( 'data-beacon_search', $invoice_beacon )
			) )->get_field(),

			( new DisableFieldProAdapter(
				'proforma_number_prefix',
				( new WPMLFieldDecorator(
					( new InputTextField() )
						->set_name( '' )
						->set_default_value( esc_html__( 'Invoice Proforma', 'flexible-invoices-core' ) )
						->set_label( esc_html__( 'Prefix', 'flexible-invoices-core' ) )
						->set_description( wp_kses( __( 'For prefixes use the following short tags: <code>{DD}</code> for day, <code>{MM}</code> for month, <code>{YYYY}</code> for year.', 'flexible-invoices-core' ), [ 'code' => [] ] ) )
						->add_class( 'hs-beacon-search' )
						->set_attribute( 'data-beacon_search', $invoice_beacon )
				) )->get_field()
			) )->get_field(),

			( new DisableFieldProAdapter(
				'proforma_number_suffix',
				( new WPMLFieldDecorator(
					( new InputTextField() )
						->set_name( '' )
						->set_default_value( '/{MM}/{YYYY}' )
						->set_label( esc_html__( 'Suffix', 'flexible-invoices-core' ) )
						->set_description( wp_kses( __( 'For suffixes use the following short tags: <code>{DD}</code> for day, <code>{MM}</code> for month, <code>{YYYY}</code> for year.', 'flexible-invoices-core' ), [ 'code' => [] ] ) )
						->add_class( 'hs-beacon-search' )
						->set_attribute( 'data-beacon_search', $invoice_beacon )
				) )->get_field()
			) )->get_field(),

			( new DisableFieldProAdapter(
				'proforma_number_reset_type',
				( new SelectField() )
					->set_name( '' )
					->set_label( esc_html__( 'Number Reset', 'flexible-invoices-core' ) )
					->set_description( esc_html__( 'Select when to reset the invoice number to 1.', 'flexible-invoices-core' ) )
					->set_options(
						[
							'year'  => esc_html__( 'Yearly', 'flexible-invoices-core' ),
							'month' => esc_html__( 'Monthly', 'flexible-invoices-core' ),
							'none'  => esc_html__( 'None', 'flexible-invoices-core' ),
						]
					)
					->set_default_value( 'month' )
					->add_class( 'hs-beacon-search' )
					->set_attribute( 'data-beacon_search', $invoice_beacon )
			) )->get_field(),

			( new DisableFieldProAdapter(
				'proforma_default_due_time',
				( new InputTextField() )
					->set_name( '' )
					->set_default_value( '0' )
					->set_attribute( 'type', 'number' )
					->set_label( esc_html__( 'Default Due Time', 'flexible-invoices-core' ) )
					->add_class( 'hs-beacon-search' )
					->set_attribute( 'data-beacon_search', $invoice_beacon )
			) )->get_field(),

			( new DisableFieldProAdapter(
				'proforma_notes',
				( new WPMLFieldDecorator(
					( new TextAreaField() )
						->set_name( '' )
						->set_label( esc_html__( 'Notes', 'flexible-invoices-core' ) )
						->add_class( 'large-text wide-input hs-beacon-search' )
						->set_attribute( 'data-beacon_search', $invoice_beacon )
				) )->get_field()
			) )->get_field(),

			( new SubEndField() )
				->set_label( '' ),
		];
	}

	/**
	 * @inheritDoc
	 */
	public static function get_tab_slug(): string {
		return 'proforma';
	}

	/**
	 * @return string
	 */
	public function get_tab_name(): string {
		return (string) esc_html__( 'Proforma', 'flexible-invoices-core' );
	}
}
