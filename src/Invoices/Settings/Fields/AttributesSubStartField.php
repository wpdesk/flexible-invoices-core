<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Settings\Fields;

/**
 * Attribute tab open field.
 *
 * @package WPDesk\Library\FlexibleInvoicesCore\Settings\Fields
 */
class AttributesSubStartField extends SubStartField {
	/**
	 * @return string
	 */
	public function get_template_name() {
		return 'attributes-sub-start';
	}
}
