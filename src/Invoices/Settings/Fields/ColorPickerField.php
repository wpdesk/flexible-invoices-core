<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Settings\Fields;

use WPDesk\Forms\Field\InputTextField;

/**
 * Color picker field.
 *
 * @package WPDesk\FIT\Settings\Fields
 */
class ColorPickerField extends InputTextField {

	/**
	 * @return string
	 */
	public function get_template_name() {
		return 'color-picker-input';
	}
}
