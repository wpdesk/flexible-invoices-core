<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Settings\Fields;

use WPDesk\Forms\Field\BasicField;

class FixedSubmitField extends BasicField {
	public function get_template_name() {
		return 'input-submit';
	}

	public function get_type() {
		return 'submit'; //@phpstan-ignore-line
	}

	public function should_override_form_template() {
		return \true;
	}
}
