<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Settings\Fields;

use WPDesk\Forms\Field\SubmitField;

/**
 * Reset settings field.
 *
 * @package WPDesk\FIT\Settings\Fields
 */
class ResetField extends SubmitField {

	/**
	 * @return string
	 */
	//@phpstan-ignore-next-line
	public function get_type() {
		return 'button';
	}
}
