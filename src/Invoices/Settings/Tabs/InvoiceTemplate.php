<?php

namespace WPDesk\Library\FlexibleInvoicesCore\Settings\Tabs;

use WPDesk\Forms\Field;
use WPDesk\Forms\Field\NoOnceField;
use WPDesk\Library\FlexibleInvoicesCore\Helpers\Hooks;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\Col;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\ColorPickerField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\DisableFieldProAdapter;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\DisableTemplateFieldAdapter;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\FICheckboxField;
use WPDesk\Forms\Field\Header;
use WPDesk\Forms\Field\SelectField;
use WPDesk\Forms\Field\SubmitField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\ResetField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\Row;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\SelectImageField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\SettingsForm;
use WPDesk\Library\FlexibleInvoicesCore\Helpers\Plugin;
use WPDesk\Library\FlexibleInvoicesCore\InvoicesIntegration;

/**
 * Invoice Template Settings Tab Page.
 *
 * @package WPDesk\Library\FlexibleInvoicesCore\Settings\Tabs
 */
final class InvoiceTemplate extends FieldSettingsTab {

	/** @var string slug od administrator role */
	const ADMIN_ROLE        = 'administrator';
	const EDITOR_ROLE       = 'editor';
	const SHOP_MANAGER_ROLE = 'shop_manager';

	/**
	 * @var string
	 */
	private $assets_url;

	public function __construct( string $assets_url ) {
		$this->assets_url = $assets_url;
	}

	/**
	 * @return array
	 */
	private function get_signature_users(): array {
		$users      = [];
		$site_users = get_users( [ 'role__in' => [ self::ADMIN_ROLE, self::EDITOR_ROLE, self::SHOP_MANAGER_ROLE ] ] );
		foreach ( $site_users as $user ) {
			$users[ $user->ID ] = $user->display_name ?: $user->user_login; //@phpstan-ignore-line
		}

		return Hooks::signature_user_filter( $users, $site_users );
	}

	/**
	 * @return string[]
	 */
	private function get_beacon_translations(): array {
		return [
			'company'     => 'Company',
			'main'        => 'Main Settings',
			'woocommerce' => 'Main Settings for WooCommerce',
		];
	}

	/**
	 * @return string
	 */
	private function get_doc_link(): string {
		$docs_link = 'https://docs.flexibleinvoices.com/article/1017-customizing-the-invoice-template?utm_source=flexible-invoices-settings&utm_medium=link&utm_campaign=flexible-invoices-docs-link&utm_content=template';
		if ( get_locale() === 'pl_PL' ) {
			$docs_link = 'https://www.wpdesk.pl/docs/faktury-woocommerce-docs/?utm_source=flexible-invoices-settings&utm_medium=link&utm_campaign=flexible-invoices-docs-link&utm_content=template#szablon-faktury';
		}
		/* translators: %1$s docs link, %2$s strong, %3$s /strong */
		$output = sprintf( '%2$s%1$s%3$s', sprintf( esc_html__( 'Read more in the %1$splugin documentation &rarr;%2$s', 'flexible-invoices-core' ), '<a href="' . $docs_link . '" target="_blank" style="color: #4BB04E; font-weight: 700;">', '</a>' ), '<strong>', '</strong>' );

		if ( get_locale() !== 'pl_PL' ) {
			$docs_link1 = 'https://docs.flexibleinvoices.com/article/790-how-to-create-custom-templates-of-invoices?utm_source=flexible-invoices-settings&utm_medium=link&utm_campaign=flexible-invoices-docs-link&utm_content=custom-template';
			$docs_link2 = 'https://docs.flexibleinvoices.com/article/789-how-to-add-custom-fields-for-generated-invoice-pdf?utm_source=flexible-invoices-settings&utm_medium=link&utm_campaign=flexible-invoices-docs-link&utm_content=custom-fields';
			$docs_link3 = 'https://docs.flexibleinvoices.com/article/791-translating-documents?utm_source=flexible-invoices-settings&utm_medium=link&utm_campaign=flexible-invoices-docs-link&utm_content=translations';
		} else {
			$docs_link1 = 'https://www.wpdesk.pl/docs/faktury-woocommerce-docs/?utm_source=flexible-invoices-settings&utm_medium=link&utm_campaign=flexible-invoices-docs-link&utm_content=custom-template#wlasny-szablon-faktury-w-folderze-motywu';
			$docs_link2 = 'https://www.wpdesk.pl/docs/faktury-woocommerce-docs/?utm_source=flexible-invoices-settings&utm_medium=link&utm_campaign=flexible-invoices-docs-link&utm_content=gtu-invoice#gtu-na-fakturach';
			$docs_link3 = '';
		}
		$output .= '<br/>';
		$output .= sprintf(
			/* translators: %1$s docs link, %2$s docs link, %3$s docs link*/
			__( 'Also, learn how to <a href="%1$s" target="_blank" style="color: #4BB04E; font-weight: 700;">adjust the invoice template</a>, add <a href="%2$s" target="_blank" style="color: #4BB04E; font-weight: 700; ">more data</a> and <a href="%3$s" target="_blank" style="color: #4BB04E; font-weight: 700; ">manage translations</a>.', 'flexible-invoices-core' ),
			$docs_link1,
			$docs_link2,
			$docs_link3
		);

		return '<span style="font-weight: 700;">' . $output . '</span>';
	}

	/**
	 * @return array|Field[]
	 */
	protected function get_fields(): array {
		$beacon  = $this->get_beacon_translations();
		$pro_url = get_locale() === 'pl_PL' ? 'https://www.wpdesk.pl/sklep/zaawansowane-szablony-faktur-woocommerce/?utm_source=wp-admin-plugins&utm_medium=button&utm_campaign=flexible-invoices-advanced-templates' : 'https://flexibleinvoices.com/products/advanced-templates-for-flexible-invoices/?utm_source=wp-admin-plugins&utm_medium=button&utm_campaign=flexible-invoices-advanced-templates';

		$pro_description = '';
		if ( Plugin::is_template_addon_is_disabled() ) {
			/* translators: %1$s link, %2$s strong, %3$s /strong */
			$pro_description = sprintf( '%2$s%1$s%3$s', sprintf( esc_html__( 'To customize PDF layout of your invoices, buy the %1$sAdvanced Sending add-on for Flexible Invoices →%2$s', 'flexible-invoices-core' ), '<a href="' . $pro_url . '" target="_blank" style="color: #8f0350; font-weight: 700;">', '</a>' ), '<strong>', '</strong>' );
			if ( ! InvoicesIntegration::is_super() ) {
				$pro_description .= '<br><span>' . esc_html__( 'The add-on requires Flexible Invoices PRO.', 'flexible-invoices-core' ) . '</span>';
			}
		}

		$color_picker_class = Plugin::is_template_addon_is_disabled() ? 'color-picker disabled' : 'color-picker';

		$fields = [
			( new Header() )
				->set_label( esc_html__( 'Invoice Template', 'flexible-invoices-core' ) )
				->set_description( $this->get_doc_link() ),
			( new FICheckboxField() )
				->set_name( 'hide_vat_number' )
				->set_label( esc_html__( 'Seller\'s VAT Number on Invoices', 'flexible-invoices-core' ) )
				->set_sublabel( esc_html__( 'If tax is 0 hide seller\'s VAT Number on PDF invoices.', 'flexible-invoices-core' ) )
				->set_attribute( 'data-beacon_search', $beacon['main'] )
				->add_class( 'hs-beacon-search' ),
			( new FICheckboxField() )
				->set_name( 'hide_vat' )
				->set_label( esc_html__( 'Tax Cells on Invoices', 'flexible-invoices-core' ) )
				->set_sublabel( esc_html__( 'If tax is 0 hide all tax cells on PDF invoices.', 'flexible-invoices-core' ) )
				->set_attribute( 'data-beacon_search', $beacon['main'] )
				->add_class( 'hs-beacon-search' ),
			( new DisableFieldProAdapter(
				'woocommerce_shipping_address',
				( new SelectField() )
					->set_name( 'shipping-address' )
					->set_label( esc_html__( 'Shipping Address', 'flexible-invoices-core' ) )
					->set_description( esc_html__( 'Enable if you want to show the customer\'s shipping address on the invoice.', 'flexible-invoices-core' ) )
					->set_options(
						[
							'none'    => esc_html__( 'Do not show', 'flexible-invoices-core' ),
							'always'  => esc_html__( 'Show customer\'s address', 'flexible-invoices-core' ),
							'ifempty' => esc_html__( 'Show customer\'s address if different from billing', 'flexible-invoices-core' ),
						]
					)
					->set_default_value( 'none' )
					->set_attribute( 'data-beacon_search', $beacon['woocommerce'] )
					->add_class( 'hs-beacon-search ' ),
				true
			) )->get_field(),
			( new FICheckboxField() )
				->set_name( 'woocommerce_get_sku' )
				->set_label( esc_html__( 'SKU', 'flexible-invoices-core' ) )
				->set_sublabel( esc_html__( 'Use SKU numbers on invoices', 'flexible-invoices-core' ) )
				->set_attribute( 'data-beacon_search', $beacon['woocommerce'] )
				->add_class( 'hs-beacon-search' ),
			( new DisableFieldProAdapter(
				'show_discount',
				( new FICheckboxField() )
					->set_name( 'discounts' )
					->set_label( esc_html__( 'Discounts', 'flexible-invoices-core' ) )
					->set_sublabel( esc_html__( 'Enable to show column with discounts on the invoice.', 'flexible-invoices-core' ) )
					->set_attribute( 'data-beacon_search', $beacon['main'] )
					->add_class( 'hs-beacon-search ' ),
				true
			) )->get_field(),
			( new FICheckboxField() )
				->set_name( 'show_signatures' )
				->set_label( esc_html__( 'Show Signatures', 'flexible-invoices-core' ) )
				->set_sublabel( esc_html__( 'Enable if you want to display place for signatures.', 'flexible-invoices-core' ) )
				->set_attribute( 'data-beacon_search', $beacon['main'] )
				->add_class( 'hs-beacon-search' ),
			( new SelectField() )
				->set_name( 'signature_user' )
				->set_label( esc_html__( 'Seller signature', 'flexible-invoices-core' ) )
				->set_description( esc_html__( 'Choose a user whose display name will be visible on the invoice in the signature section.', 'flexible-invoices-core' ) )
				->set_options(
					$this->get_signature_users()
				)
				->set_attribute( 'data-beacon_search', $beacon['main'] )
				->add_class( 'hs-beacon-search' ),
			( new FICheckboxField() )
				->set_name( 'pdf_numbering' )
				->set_label( esc_html__( 'PDF Numbering', 'flexible-invoices-core' ) )
				->set_sublabel( esc_html__( 'Enable page numbering.', 'flexible-invoices-core' ) )
				->set_attribute( 'data-beacon_search', $beacon['main'] )
				->add_class( 'hs-beacon-search' ),
			( new DisableTemplateFieldAdapter(
				'template_headers',
				( new Header() )
					->set_name( '' )
					->set_label( __( 'Advanced Invoice Template', 'flexible-invoices-core' ) )
					// translators: %1$s url, %2$s label.
					->set_description( $pro_description )
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_layout',
				( new SelectImageField() )
					->set_name( '' )
					->set_label( __( 'Layout', 'flexible-invoices-core' ) )
					->set_options(
						$this->get_layouts()
					)->set_default_value( 'default' )
					->set_attribute( 'is_disabled', Plugin::is_template_addon_is_disabled() ? 'yes' : 'no' )
			) )->get_field(),
			( new Row() )->set_name( 'row_open' ),
			( new DisableTemplateFieldAdapter(
				'template_text',
				( new Header() )
					->set_name( '' )
					->set_label( __( 'Text', 'flexible-invoices-core' ) )
					->set_description( __( 'Document body text.', 'flexible-invoices-core' ) )
					->set_header_size( '3' )
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_text_font_family',
				( new SelectField() )
					->set_name( '' )
					->set_default_value( 'dejavusanscondensed' )
					->set_attribute( 'data-default_value', 'dejavusanscondensed' )
					->set_options(
						$this->font_families()
					)
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_text_font_size',
				( new SelectField() )
					->set_name( '' )
					->set_default_value( '8' )
					->set_attribute( 'data-default_value', '8' )
					->set_options(
						$this->text_font_sizes()
					)
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_text_font_color',
				( new ColorPickerField() )
					->set_name( '' )
					->set_default_value( '#000000' )
					->set_attribute( 'data-default_value', '#000000' )
					->add_class( $color_picker_class )
			) )->get_field(),
			( new Col() )->set_name( 'col_open' ),
			( new DisableTemplateFieldAdapter(
				'template_heading1',
				( new Header() )
					->set_name( '' )
					->set_label( __( 'Heading 1', 'flexible-invoices-core' ) )
					->set_description( __( 'Invoice number.', 'flexible-invoices-core' ) )
					->set_header_size( '3' )
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_heading1_font_family',
				( new SelectField() )
					->set_name( '' )
					->set_default_value( 'dejavusanscondensed' )
					->set_attribute( 'data-default_value', 'dejavusanscondensed' )
					->set_options(
						$this->font_families()
					)
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_heading1_font_size',
				( new SelectField() )
					->set_name( '' )
					->set_default_value( '18' )
					->set_attribute( 'data-default_value', '18' )
					->set_options(
						$this->header_font_sizes()
					)
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_heading1_font_color',
				( new ColorPickerField() )
					->set_name( '' )
					->set_default_value( '#000000' )
					->set_attribute( 'data-default_value', '#000000' )
					->add_class( $color_picker_class )
			) )->get_field(),
			( new Col() )->set_name( 'col_open' ),
			( new DisableTemplateFieldAdapter(
				'template_heading2',
				( new Header() )
					->set_name( '' )
					->set_label( __( 'Heading 2', 'flexible-invoices-core' ) )
					->set_description( __( 'Section headers.', 'flexible-invoices-core' ) )
					->set_header_size( '3' )
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_heading2_font_family',
				( new SelectField() )
					->set_name( '' )
					->set_default_value( 'dejavusanscondensed' )
					->set_attribute( 'data-default_value', 'dejavusanscondensed' )
					->set_options(
						$this->font_families()
					)
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_heading2_font_size',
				( new SelectField() )
					->set_name( '' )
					->set_default_value( '12' )
					->set_attribute( 'data-default_value', '12' )
					->set_options(
						$this->header_font_sizes()
					)
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_heading2_font_color',
				( new ColorPickerField() )
					->set_name( '' )
					->set_default_value( '#000000' )
					->set_attribute( 'data-default_value', '#000000' )
					->add_class( $color_picker_class )
			) )->get_field(),
			( new Col() )->set_name( 'col_open' ),
			( new DisableTemplateFieldAdapter(
				'template_heading3',
				( new Header() )
					->set_name( '' )
					->set_label( __( 'Heading 3', 'flexible-invoices-core' ) )
					->set_description( __( 'Names of columns in the table.', 'flexible-invoices-core' ) )
					->set_header_size( '3' )
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_heading3_font_family',
				( new SelectField() )
					->set_name( '' )
					->set_default_value( 'dejavusanscondensed' )
					->set_attribute( 'data-default_value', 'dejavusanscondensed' )
					->set_options(
						$this->font_families()
					)
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_heading3_font_size',
				( new SelectField() )
					->set_name( '' )
					->set_default_value( '9' )
					->set_attribute( 'data-default_value', '9' )
					->set_options(
						$this->text_font_sizes()
					)
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_heading3_font_color',
				( new ColorPickerField() )
					->set_name( '' )
					->set_default_value( '#000000' )
					->set_attribute( 'data-default_value', '#000000' )
					->add_class( $color_picker_class )
			) )->get_field(),
			( new Row( false ) )->set_name( 'row-close' ),
			( new DisableTemplateFieldAdapter(
				'template_table_header',
				( new Header() )
					->set_name( '' )
					->set_label( __( 'Table design', 'flexible-invoices-core' ) )
					->set_description( __( 'Customize table element styles.', 'flexible-invoices-core' ) )
					->set_header_size( '3' )
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_table_border_size',
				( new SelectField() )
					->set_name( '' )
					->set_label( __( 'Table border thickness', 'flexible-invoices-core' ) )
					->set_default_value( '1' )
					->set_attribute( 'data-default_value', '1' )
					->set_options(
						$this->border_sizes()
					)
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_table_border_color',
				( new ColorPickerField() )
					->set_name( '' )
					->set_label( __( 'Table border color', 'flexible-invoices-core' ) )
					->set_default_value( '#000000' )
					->set_attribute( 'data-default_value', '#000000' )
					->add_class( $color_picker_class )
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_table_header_bg',
				( new ColorPickerField() )
					->set_name( '' )
					->set_label( __( 'Table header background', 'flexible-invoices-core' ) )
					->set_default_value( '#F1F1F1' )
					->set_attribute( 'data-default_value', '#F1F1F1' )
					->add_class( $color_picker_class )
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_table_rows_even',
				( new ColorPickerField() )
					->set_name( '' )
					->set_label( __( 'Rows color (even)', 'flexible-invoices-core' ) )
					->set_default_value( '#FFFFFF' )
					->set_attribute( 'data-default_value', '#FFFFFF' )
					->add_class( $color_picker_class )
			) )->get_field(),
			( new DisableTemplateFieldAdapter(
				'template_reset_settings',
				( new ResetField() )
					->set_label( __( 'Reset appearance', 'flexible-invoices-core' ) )
					->set_name( '' )
					->add_class( 'reset-pdf-template button-secondary' )
			) )->get_field(),

			( new NoOnceField( SettingsForm::NONCE_ACTION ) )
				->set_name( SettingsForm::NONCE_NAME ),
			( new SubmitField() )
				->set_name( 'save' )
				->set_label( esc_html__( 'Save changes', 'flexible-invoices-core' ) )
				->add_class( 'button-primary' ),
		];

		/**
		 * Filters invoice template settings fields.
		 *
		 * @param array $fields Collection of fields.
		 * @param array $beacon Beacon strings.
		 *
		 * @since 2.0.0
		 */
		return apply_filters( 'fi/core/settings/tabs/invoice_template/fields', $fields, $beacon );
	}

	/**
	 * @return string[]
	 */
	public function border_sizes(): array {
		$n = [];
		for ( $i = 1; $i <= 4; $i++ ) {
			$n[ $i ] = $i . 'px';
		}

		return $n;
	}

	/**
	 * @return string[]
	 */
	public function text_font_sizes(): array {
		$n = [];
		for ( $i = 8; $i <= 12; $i++ ) {
			$n[ $i ] = $i . 'px';
		}

		return $n;
	}

	/**
	 * @return string[]
	 */
	public function header_font_sizes(): array {
		$n = [];
		for ( $i = 10; $i <= 32; $i++ ) {
			if ( $i % 2 !== 0 ) {
				continue;
			}
			$n[ $i ] = $i . 'px';
		}

		return $n;
	}

	/**
	 * @return string[]
	 */
	public function font_families(): array {
		return [
			'dejavusans'          => 'DeJaVu Sans',
			'dejavuserif'         => 'DeJaVu Serif',
			'dejavusanscondensed' => 'DeJaVu Sans Condensed',
			'freeserif'           => 'FreeSerif',
			'montserrat'          => 'Montserrat',
			'opensans'            => 'OpenSans',
			'opensanscondensed'   => 'OpenSansCondensed',
			'roboto'              => 'Roboto',
			'robotoslab'          => 'RobotoSlab',
			'rubik'               => 'Rubik',
			'titilliumweb'        => 'TitilliumWeb',
		];
	}

	/**
	 * @return array
	 */
	private function get_layouts(): array {
		return [
			'default' => [
				'name'      => __( 'Default', 'flexible-invoices-core' ),
				'thumb_src' => $this->assets_url . 'images/template1_min.jpg',
				'large_src' => $this->assets_url . 'images/template1.jpg',
			],
			'layout1' => [
				'name'      => __( 'Layout no. 1', 'flexible-invoices-core' ),
				'thumb_src' => $this->assets_url . 'images/template2_min.jpg',
				'large_src' => $this->assets_url . 'images/template2.jpg',
			],
			'layout2' => [
				'name'      => __( 'Layout no. 2', 'flexible-invoices-core' ),
				'thumb_src' => $this->assets_url . 'images/template3_min.jpg',
				'large_src' => $this->assets_url . 'images/template3.jpg',
			],
			'layout3' => [
				'name'      => __( 'Layout no. 3', 'flexible-invoices-core' ),
				'thumb_src' => $this->assets_url . 'images/template4_min.jpg',
				'large_src' => $this->assets_url . 'images/template4.jpg',
			],
		];
	}

	/**
	 * @return string
	 */
	public static function get_tab_slug(): string {
		return 'invoice-template';
	}

	/**
	 * @return string
	 */
	public function get_tab_name(): string {
		return esc_html__( 'Invoice Template', 'flexible-invoices-core' );
	}
}
