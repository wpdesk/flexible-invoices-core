<?php
/**
 * Woocommerce Settings.
 *
 * @package WPDesk\FlexibleInvoicesWooCommerce
 */

namespace WPDesk\Library\FlexibleInvoicesCore\Settings\WooCommerceFields;

use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\FICheckboxField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\SubEndField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\SubStartField;
use WPDesk\Forms\Field\Header;
use WPDesk\Forms\Field\InputTextField;

/**
 * Checkout settings subpage.
 *
 * @package WPDesk\Library\FlexibleInvoicesCore\Settings\WooCommerceFields
 */
final class CheckoutSettingsFields implements SubTabInterface {

	/**
	 * @inheritDoc
	 */
	public function get_fields(): array {
		$plugin_url = get_locale() === 'pl_PL' ? 'https://www.wpdesk.pl/sklep/woocommerce-checkout-fields/' : 'https://www.wpdesk.net/products/flexible-checkout-fields-pro/';

		$plugin_url .= '?utm_source=flexible-invoices-settings&utm_medium=link&utm_campaign=flexible-checkout-fields';

		$checkout = 'Checkout form';

		return [
			( new SubStartField() )
				->set_label( esc_html__( 'Checkout', 'flexible-invoices-core' ) )
				->set_name( 'checkout' ),
			( new Header() )
				->set_label( esc_html__( 'Checkout', 'flexible-invoices-core' ) )
				// translators: %s docs url.
				->set_description( sprintf( __( 'Warning. If you use a plugin for editing <a href="%s">checkout fields</a> it may override the following settings.', 'flexible-invoices-core' ), $plugin_url ) ),
			( new FICheckboxField() )
				->set_name( 'woocommerce_add_invoice_ask_field' )
				->set_label( esc_html__( 'Ask the customer if he wants an invoice', 'flexible-invoices-core' ) )
				->set_sublabel( esc_html__( 'Enable', 'flexible-invoices-core' ) )
				->set_description( esc_html__( 'If enabled the customer can choose to get an invoice. If automatic sending is enabled invoices will be issued only for these orders.', 'flexible-invoices-core' ) )
				->set_attribute( 'data-beacon_search', $checkout )
				->add_class( 'hs-beacon-search' ),
			( new FICheckboxField() )
				->set_name( 'woocommerce_add_nip_field' )
				->set_label( esc_html__( 'Add VAT Number field to checkout', 'flexible-invoices-core' ) )
				->set_sublabel( esc_html__( 'Enable', 'flexible-invoices-core' ) )
				->set_attribute( 'data-beacon_search', $checkout )
				->add_class( 'hs-beacon-search' ),
			( new InputTextField() )
				->set_name( 'woocommerce_nip_label' )
				->set_label( esc_html__( 'Label', 'flexible-invoices-core' ) )
				->set_default_value( esc_html__( 'VAT Number', 'flexible-invoices-core' ) )
				->add_class( 'nip-additional-fields hs-beacon-search' )
				->set_attribute( 'data-beacon_search', $checkout ),
			( new InputTextField() )
				->set_name( 'woocommerce_nip_placeholder' )
				->set_label( esc_html__( 'Placeholder', 'flexible-invoices-core' ) )
				->set_placeholder( esc_html__( 'VAT Number', 'flexible-invoices-core' ) )
				->add_class( 'nip-additional-fields hs-beacon-search' )
				->set_attribute( 'data-beacon_search', $checkout ),
			( new FICheckboxField() )
				->set_name( 'woocommerce_nip_required' )
				->set_label( esc_html__( 'VAT Number field required', 'flexible-invoices-core' ) )
				->set_sublabel( esc_html__( 'Enable', 'flexible-invoices-core' ) )
				->add_class( 'nip-additional-fields hs-beacon-search' )
				->set_attribute( 'data-beacon_search', $checkout ),
			( new FICheckboxField() )
				->set_name( 'woocommerce_validate_nip' )
				->set_label( esc_html__( 'Validate VAT Number', 'flexible-invoices-core' ) )
				->set_sublabel( esc_html__( 'Enable', 'flexible-invoices-core' ) )
				->set_description( esc_html__( 'VAT Number will have to be entered without hyphens, spaces and optionally can be prefixed with country code.', 'flexible-invoices-core' ) )
				->add_class( 'nip-additional-fields hs-beacon-search' )
				->set_attribute( 'data-beacon_search', $checkout ),
			( new SubEndField() )->set_label( '' ),
		];
	}

	/**
	 * @return string
	 */
	public static function get_tab_slug(): string {
		return 'checkout';
	}

	/**
	 * @return string
	 */
	public function get_tab_name(): string {
		return esc_html__( 'Checkout', 'flexible-invoices-core' );
	}
}
