<?php
/**
 * Woocommerce Settings.
 *
 * @package WPDesk\FlexibleInvoicesWooCommerce
 */

namespace WPDesk\Library\FlexibleInvoicesCore\Settings\WooCommerceFields;

use WC_Tax;
use WPDesk\Forms\Field;
use WPDesk\Forms\Field\WooSelect;
use WPDesk\Library\FlexibleInvoicesCore\Helpers\Plugin;
use WPDesk\Library\FlexibleInvoicesCore\InvoicesIntegration;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\DisableFieldProAdapter;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\FICheckboxField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\SubEndField;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Fields\SubStartField;
use WPDesk\Library\FlexibleInvoicesCore\SettingsStrategy\SettingsStrategy;
use WPDesk\Forms\Field\Header;
use WPDesk\Forms\Field\InputTextField;
use WPDesk\Forms\Field\SelectField;

/**
 * Moss settings subpage.
 *
 * @package WPDesk\Library\FlexibleInvoicesCore\Settings\WooCommerceFields
 */
final class MossSettingsFields implements SubTabInterface {

	/**
	 * @return array
	 */
	private function get_woocommerce_tax_classes(): array {
		$tax_classes                 = WC_Tax::get_tax_classes();
		$classes_options             = [];
		$classes_options['standard'] = esc_html__( 'Standard', 'flexible-invoices-core' );
		foreach ( $tax_classes as $class ) {
			$classes_options[ sanitize_title( $class ) ] = esc_html( $class );
		}

		return $classes_options;
	}

	/**
	 * @return string
	 */
	private function get_moss_link(): string {
		$docs_url = 'https://wpde.sk/flexible-invoices-oss';
		if ( get_locale() === 'pl_PL' ) {
			$docs_url = 'https://wpde.sk/faktury-woocommerce-oss';
		}

		/* translators: %1$s - open tag, %2$s - close tag */
		return sprintf( '%2$s%1$s%3$s', sprintf( esc_html__( 'Read more in the %1$splugin documentation &rarr;%2$s', 'flexible-invoices-core' ), '<a href="' . $docs_url . '" target="_blank" style="color: #4BB04E; font-weight: 700;">', '</a>' ), '<strong>', '</strong>' );
	}

	/**
	 * @return string
	 */
	private function get_doc_link(): string {
		if ( InvoicesIntegration::is_super() ) {
			return __( 'The EU OSS procedure is an extension of MOSS. From 07.2021 VAT on every transaction above €10.000 to other EU countries must be calculated based on the customer location, and you need to collect evidence of this (IP address and Billing Address). B2B transactions are subject to reverse charge.', 'flexible-invoices-core' ) . ' ' . $this->get_moss_link();
		} else {
			return sprintf(
				'<a href="%1$s&utm_content=oss" target="_blank" style="color: #8f0350; font-weight: 700;">%2$s</a>',
				Plugin::upgrade_to_pro_url(),
				esc_html__( 'Upgrade to PRO and enable options below →', 'flexible-invoices-core' )
			);
		}
	}

	/**
	 * @return array|Field[]
	 */
	public function get_fields(): array {
		$moss = 'MOSS';

		return [
			( new SubStartField() )
				->set_label( esc_html__( 'OSS', 'flexible-invoices-core' ) )
				->set_name( 'moss' ),
			( new Header() )
				->set_label( esc_html__( 'OSS Handling', 'flexible-invoices-core' ) )
				// translators: %s docs url.
				->set_description( $this->get_doc_link() ),
			( new DisableFieldProAdapter(
				'woocommerce_eu_vat_vies_validate',
				( new FICheckboxField() )
					->set_name( '' )
					->set_label( esc_html__( 'VIES Validation', 'flexible-invoices-core' ) )
					->set_sublabel( esc_html__( 'Enable', 'flexible-invoices-core' ) )
					->add_class( 'hs-beacon-search woocommerce_eu_vat_vies_validate' )
					->set_attribute( 'data-beacon_search', $moss )
			) )->get_field(),

			( new DisableFieldProAdapter(
				'woocommerce_eu_vat_failure_handling',
				( new SelectField() )
					->set_name( '' )
					->set_label( esc_html__( 'Failed Validation Handling', 'flexible-invoices-core' ) )
					->set_options(
						[
							'reject'             => esc_html__( 'Reject the order and show the customer an error message.', 'flexible-invoices-core' ),
							'accept_with_vat'    => esc_html__( 'Accept the order, but do not remove VAT.', 'flexible-invoices-core' ),
							'accept_without_vat' => esc_html__( 'Accept the order and remove VAT.', 'flexible-invoices-core' ),
						]
					)
					->add_class( 'vies-validation-fields hs-beacon-search' )
					->set_attribute( 'data-beacon_search', $moss )
			) )->get_field(),

			( new DisableFieldProAdapter(
				'woocommerce_moss_tax_classes',
				( new WooSelect() )
					->set_name( '' )
					->set_label( esc_html__( 'Tax class for OSS', 'flexible-invoices-core' ) )
					->set_description( esc_html__( 'Select the tax classes that the plugin shall use to handling the OSS.', 'flexible-invoices-core' ) )
					->set_options(
						$this->get_woocommerce_tax_classes()
					)
					->set_attribute( 'multiple', 'multiple' )
					->add_class( 'select2 vies-validation-fields hs-beacon-search' )
					->set_attribute( 'data-beacon_search', $moss )
			) )->get_field(),

			( new DisableFieldProAdapter(
				'woocommerce_moss_validate_ip',
				( new FICheckboxField() )
					->set_name( '' )
					->set_label( esc_html__( 'Collect and Validate Evidence', 'flexible-invoices-core' ) )
					->set_sublabel( esc_html__( 'Enable', 'flexible-invoices-core' ) )
					->set_description( esc_html__( 'Option validates the customer IP address against their billing address, and prompts the customer to self-declare their address if they do not match.', 'flexible-invoices-core' ) )
					->add_class( 'vies-validation-fields hs-beacon-search' )
					->set_attribute( 'data-beacon_search', $moss )
			) )->get_field(),

			( new DisableFieldProAdapter(
				'woocommerce_reverse_charge_description',
				( new InputTextField() )
					->set_name( '' )
					->set_label( esc_html__( 'Reverse charge description', 'flexible-invoices-core' ) )
					->set_default_value( esc_html__( 'Reverse charge', 'flexible-invoices-core' ) )
					->add_class( 'vies-validation-fields hs-beacon-search' )
					->set_attribute( 'data-beacon_search', $moss )
			) )->get_field(),

			( new DisableFieldProAdapter(
				'woocommerce_vat_moss_description',
				( new InputTextField() )
					->set_name( '' )
					->set_label( esc_html__( 'VAT OSS rate description', 'flexible-invoices-core' ) )
					->add_class( 'vies-validation-fields hs-beacon-search' )
					->set_attribute( 'data-beacon_search', $moss )
			) )->get_field(),

			( new SubEndField() )->set_label( '' ),
		];
	}

	/**
	 * @return string
	 */
	public static function get_tab_slug(): string {
		return 'moss';
	}

	/**
	 * @return string
	 */
	public function get_tab_name(): string {
		return esc_html__( 'OSS', 'flexible-invoices-core' );
	}
}
