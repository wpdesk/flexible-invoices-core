<?php

namespace WPDesk\Library\FlexibleInvoicesCore\WooCommerce;

use WPDesk\Library\FlexibleInvoicesAbstracts\Documents\Document;
use WPDesk\Library\FlexibleInvoicesCore\Settings\Settings;
use WPDesk\PluginBuilder\Plugin\Hookable;


class OrderPaymentUrl implements Hookable {

	/**
	 * @var Settings
	 */
	private $settings;

	/**
	 * @param Settings $settings
	 */
	public function __construct( Settings $settings ) {
		$this->settings = $settings;
	}

	/**
	 * Fires hooks.
	 */
	public function hooks() {
		if ( 'yes' === $this->settings->get( 'woocommerce_add_order_url' ) ) {
			add_action( 'fi/core/template/invoice/after_notes', [ $this, 'add_payment_url' ] );
		}
	}

	/**
	 * @param Document $document
	 *
	 * @return void
	 */
	public function add_payment_url( Document $document ): void {
		$order_id = $document->get_order_id();
		$order    = wc_get_order( $order_id );
		if ( $order ) {
			$order_status = $order->get_status();
			if ( $document->get_payment_status() !== 'paid' && ( $order_status === 'on-hold' || $order_status === 'pending' || $order_status === 'failed' ) ) {
				$pay_label = apply_filters( 'fi/core/template/payment/label', esc_html__( 'Pay for this order', 'flexible-invoices-core' ) );
				echo '<a href="' . esc_url( $order->get_checkout_payment_url() ) . '" target="_blank">' . esc_html( $pay_label ) . '</a>';
			}
		}
	}
}
