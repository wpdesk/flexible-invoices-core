<?php
/**
 * Get document from wp tables
 *
 * @package WPDesk\Library\FlexibleInvoicesCore
 */

namespace WPDesk\Library\FlexibleInvoicesCore\WordPress;

/**
 * @package WPDesk\Library\FlexibleInvoicesCore\WordPress
 */
class InvoiceQuery {

	/**
	 * @param $type
	 *
	 * @return \WP_Query
	 */
	public function get_documents_by_type( $type ): \WP_Query {
		$args = [
			'post_type'      => 'inspire_invoice',
			'meta_key'       => '_type', // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_key
			'meta_value'     => $type, // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_value
			'posts_per_page' => 50,
		];

		return new \WP_Query( $args );
	}


	/**
	 * @param int $id
	 *
	 * @return array|\WP_Post|null
	 */
	public function get_document_by_id( int $id ) {
		if ( ! $id ) {
			return [];
		}

		return get_post( $id );
	}
}
