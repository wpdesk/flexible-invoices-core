<?php

namespace WPDesk\Library\FlexibleInvoicesCore\WordPress;

use WPDesk\Library\FlexibleInvoicesCore\PDF\GeneratePDF;

/**
 * Generate & download PDF.
 *
 * This class exists only for backward compatibility.
 */
class PDF extends GeneratePDF {

}
