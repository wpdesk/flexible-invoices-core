<?php
/**
 * File: parts/footer.php
 */

use WPDesk\Library\FlexibleInvoicesAbstracts\Documents\Document;
use WPDesk\Library\FlexibleInvoicesCore\Helpers\Hooks;
use WPDesk\Library\FlexibleInvoicesCore\Helpers\Template;

$layout_name = isset( $layout_name ) ? $layout_name : 'default';
?>
<table id="footer" class="table-without-margin" style="margin-top: 10px;">
    <tr>
        <td style="text-align: <?php echo esc_attr( Template::rtl_align( 'left' ) ); ?>;">
			<?php $note = $invoice->get_notes(); ?>
			<?php if ( ! empty( $note ) ): ?>
				<p><strong><?php esc_html_e( 'Notes', 'flexible-invoices-core' ); ?></strong></p>
				<p><?php echo str_replace( PHP_EOL, '<br/>', esc_html( $note ) ); ?></p>
			<?php endif; ?>
            <?php
            Hooks::template_invoice_after_notes( $invoice, $client_country, $hideVat, $hideVatNumber );
            ?>

			<?php if ( $invoice->get_show_order_number() ): ?>
				<?php $order = $invoice->get_order_number(); ?>
				<p><?php esc_html_e( 'Order number', 'flexible-invoices-core' ); ?>: <?php echo esc_html( $invoice->get_order_number() ); ?></p>
			<?php endif; ?>
        </td>
    </tr>
</table>
