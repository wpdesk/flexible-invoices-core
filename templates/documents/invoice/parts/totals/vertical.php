<?php
/**
 * File: parts/totals.php
 */

use WPDesk\Library\FlexibleInvoicesCore\Helpers\CalculateTotals;

?>
<table style="float: <?php echo \WPDesk\Library\FlexibleInvoicesCore\Helpers\Template::rtl_align( 'right' ); ?>;">
	<tbody>
	<tr>
		<td>
			<?php esc_html_e( 'Total', 'flexible-invoices-core' ); ?>:
		</td>
		<td style="text-align: <?php echo \WPDesk\Library\FlexibleInvoicesCore\Helpers\Template::rtl_align( 'right' ); ?>">
			<strong><?php echo esc_html( $helper->string_as_money( $invoice->get_total_gross() ) ); ?></strong>
		</td>
	</tr>
	<?php if( $invoice->get_type() !== 'proforma' ): ?>
	<tr>
		<td>
			<?php esc_html_e( 'Paid', 'flexible-invoices-core' ); ?>:
		</td>
		<td style="text-align: <?php echo \WPDesk\Library\FlexibleInvoicesCore\Helpers\Template::rtl_align( 'right' ); ?>">
			<strong><?php echo esc_html( $helper->string_as_money( $invoice->get_total_paid() ) ); ?></strong>
		</td>
	</tr>
	<?php endif; ?>
	<tr>
		<td>
			<?php esc_html_e( 'Due', 'flexible-invoices-core' ); ?>:
		</td>
		<td style="text-align: <?php echo \WPDesk\Library\FlexibleInvoicesCore\Helpers\Template::rtl_align( 'right' ); ?>">
			<strong><?php echo esc_html( $helper->string_as_money( CalculateTotals::calculate_due_price( $invoice->get_total_gross(), $invoice->get_total_paid() ) ) ); ?></strong>
		</td>
	</tr>
	</tbody>
</table>
