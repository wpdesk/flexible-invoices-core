<?php

namespace Tests\Invoices\Documents;

use PHPUnit\Framework\TestCase;
use WPDesk\Library\FlexibleInvoicesAbstracts\ValueObjects\DocumentCustomer;
use WPDesk\Library\FlexibleInvoicesAbstracts\ValueObjects\DocumentSeller;
use WPDesk\Library\FlexibleInvoicesCore\Documents\Invoice;

class InvoiceTest extends TestCase {

    /**
     * @var Invoice
     */
    public $invoice;

    const DISCOUNT = 10.00;
    const ORDER_ID = 100;
    const CURRENCY = 'PLN';
    const TOTAL_TAX = 123.00;
    const TOTAL_NET = 1000.00;
    const TOTAL_GROSS = 1123.00;
    const TOTAL_PAID = 1123.00;
    const PAYMENT_METHOD = 'bacs';

    public function setUp() {
        parent::setUp();
        $this->invoice = ( new Invoice() );
        $this->invoice->set_discount( self::DISCOUNT );
        $this->invoice->set_order_id( self::ORDER_ID );
        $this->invoice->set_currency( self::CURRENCY );
        $this->invoice->set_total_tax( self::TOTAL_TAX );
        $this->invoice->set_total_gross( self::TOTAL_GROSS );
        $this->invoice->set_total_net( self::TOTAL_NET );
        $this->invoice->set_total_paid( self::TOTAL_PAID );
        $this->invoice->set_payment_method( self::PAYMENT_METHOD );
        $this->invoice->set_customer_filter_field( 'Andrzej' );
        $this->invoice->set_customer( $this->get_customer() );
        $this->invoice->set_payment_method_name( 'PLN' );
        $this->invoice->set_date_of_paid( time() );
        $this->invoice->set_date_of_sale( time() );
        $this->invoice->set_date_of_issue( time() );
        $this->invoice->set_date_of_pay( time() );
        $this->invoice->set_number( 10 );
        $this->invoice->set_formatted_number( 'Invoice 10/05/2020' );
        $this->invoice->set_seller( $this->get_seller() );
        $this->invoice->set_notes( 'PLN' );
        $this->invoice->set_user_lang( 'pl_PL' );
        $this->invoice->set_items( [] );
        $this->invoice->set_id( 1 );
        $this->invoice->set_show_order_number( true );
        $this->invoice->set_payment_status( 'paid' );
    }

    /**
     * @return DocumentCustomer
     */
    private function get_customer(): DocumentCustomer {
        return new DocumentCustomer( 1, 'Anna Nowak', 'Zawiszy 14/155', '01-160', 'Warszawa', '795678920', 'PL', '796778890', 'anna.nowak@gmail.com' );
    }

    /**
     * @return DocumentSeller
     */
    private function get_seller(): DocumentSeller {
        return new DocumentSeller( 1, '', 'Anna Nowak', 'Zawiszy 14/155', '795678920', 'Santander', '1090 9029 0992 0929 02921', 'Zbigniew Wichura' );
    }

    public function testShouldCheckInvoiceData() {
        $value = $this->invoice->get_type();
        $this->assertSame( $value, Invoice::DOCUMENT_TYPE );

        $value = $this->invoice->get_discount();
        $this->assertSame( $value, self::DISCOUNT );

        $value = $this->invoice->get_order_id();
        $this->assertSame( $value, self::ORDER_ID );

        $value = $this->invoice->get_currency();
        $this->assertSame( $value, self::CURRENCY );

        $value = $this->invoice->get_total_tax();
        $this->assertSame( $value, self::TOTAL_TAX );

        $value = $this->invoice->get_total_gross();
        $this->assertSame( $value, self::TOTAL_GROSS );

        $value = $this->invoice->get_total_net();
        $this->assertSame( $value, self::TOTAL_NET );

        $value = $this->invoice->get_total_paid();
        $this->assertSame( $value, self::TOTAL_PAID );

        $value = $this->invoice->get_payment_method();
        $this->assertSame( $value, self::PAYMENT_METHOD );
    }


}
