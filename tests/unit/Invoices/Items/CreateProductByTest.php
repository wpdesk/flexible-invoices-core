<?php

namespace Tests\Invoices\Items;

use PHPUnit\Framework\TestCase;
use WPDesk\Library\FlexibleInvoicesCore\Documents\CreateDocumentItem;
use WPDesk\Library\FlexibleInvoicesCore\Documents\Items\ProductItem;
use WPDesk\Library\FlexibleInvoicesCore\Documents\Items\ShippingItem;

class CreateProductByTest extends TestCase {

    public function testSchouldThrowExceptionIfTypeIsInvalid() {
        $this->expectException( \Exception::class );
        $this->expectExceptionMessage( 'Unknown item type! Choose from: product, shipping, discount, fee' );
        ( new CreateDocumentItem( 'boczek' ) )->net_price( 10000.00, 23 );
    }

    /**
     * @return array
     */
    private function netPriceItemData(): array {
        return [
            'type'               => 'product',
            'name'               => 'OneWheel XR+',
            'unit'               => 'item',
            'quantity'           => 1.0,
            'net_price'          => 10000.00,
            'discount'           => 0.0,
            'net_price_sum'      => 10000.00,
            'vat_rate'           => 23.0,
            'vat_sum'            => 2300.00,
            'vat_type'           => 23.0,
            'vat_type_name'      => '23%',
            'vat_type_index'     => 0,
            'total_price'        => 12300.00,
            'sku'                => 'OWXRPlus',
            'product_attributes' => [],
            'item_meta'          => [],
        ];
    }


    public function testSchouldCreateProductFromNetPrice() {
        $item = ( new CreateDocumentItem( ProductItem::TYPE ) )->net_price( 10000.00, 23 );
        $item->set_name( 'OneWheel XR+' );
        $item->set_sku( 'OWXRPlus' );
        $this->assertSame( $item->get(), $this->netPriceItemData() );
    }

    /**
     * @return array
     */
    private function netPriceItemDataWithoutVat(): array {
        return [
            'type'               => 'product',
            'name'               => 'OneWheel XR+',
            'unit'               => 'item',
            'quantity'           => 2.0,
            'net_price'          => 199.99,
            'discount'           => 0.0,
            'net_price_sum'      => 399.98,
            'vat_rate'           => 0.0,
            'vat_sum'            => 0.0,
            'vat_type'           => 0.0,
            'vat_type_name'      => '0%',
            'vat_type_index'     => 0,
            'total_price'        => 399.98,
            'sku'                => 'OWXRPlus',
            'product_attributes' => [],
            'item_meta'          => [],
        ];
    }

    public function testSchouldCreateProductFromNetPriceWithOutVat() {
        $item = ( new CreateDocumentItem( ProductItem::TYPE ) )->net_price( 199.99, 0, 2 );
        $item->set_name( 'OneWheel XR+' );
        $item->set_sku( 'OWXRPlus' );
        $this->assertSame( $item->get(), $this->netPriceItemDataWithoutVat() );
    }


    /**
     * @return array
     */
    private function grossPriceItemData(): array {
        return [
            'type'               => 'shipping',
            'name'               => 'Fedex Domestic',
            'unit'               => 'item',
            'quantity'           => 1.0,
            'net_price'          => 10000.00,
            'discount'           => 0.0,
            'net_price_sum'      => 10000.00,
            'vat_rate'           => 23.0,
            'vat_sum'            => 2300.00,
            'vat_type'           => 23.0,
            'vat_type_name'      => '23%',
            'vat_type_index'     => 0,
            'total_price'        => 12300.00,
            'sku'                => '',
            'product_attributes' => [],
            'item_meta'          => [],
        ];
    }


    public function testSchouldCreateProductFromGrossPrice() {
        $item = ( new CreateDocumentItem( ShippingItem::TYPE ) )->gross_price( 12300.00, 23 );
        $item->set_name( 'Fedex Domestic' );
        $item->set_sku( '' );
        $this->assertSame( $item->get(), $this->grossPriceItemData() );
    }

}
