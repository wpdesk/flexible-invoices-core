<?php

namespace Tests\Invoices\Items;

use PHPUnit\Framework\TestCase;
use WPDesk\Library\FlexibleInvoicesCore\Documents\Items\FeeItem;

class FeeItemTest extends TestCase {

    /**
     * @return array
     */
    private function emptyItemData(): array {
        return [
            'type'               => 'fee',
            'name'               => 'Pay Pro',
            'unit'               => 'item',
            'quantity'           => 1.0,
            'net_price'          => 0.0,
            'discount'           => 0.0,
            'net_price_sum'      => 0.0,
            'vat_rate'           => 0.0,
            'vat_sum'            => 0.0,
            'vat_type'           => 0,
            'vat_type_name'      => '0%',
            'vat_type_index'     => 0,
            'total_price'        => 0.0,
            'sku'                => '',
            'product_attributes' => [],
            'item_meta'          => [],
        ];
    }

    public function testShouldCreateEmptyItem() {
        $item = ( new FeeItem() )->set_name( 'Pay Pro' )->get();
        $this->assertEquals( $item, $this->emptyItemData() );
    }

    /**
     * @return array
     */
    private function filledItemData(): array {
        return [
            'type'               => 'fee',
            'name'               => 'PayU',
            'unit'               => 'item',
            'quantity'           => 2.0,
            'net_price'          => 100.0,
            'discount'           => 0.0,
            'net_price_sum'      => 200.0,
            'vat_rate'           => 23.0,
            'vat_sum'            => 46.00,
            'vat_type'           => 23.0,
            'vat_type_name'      => '23%',
            'vat_type_index'     => 0,
            'total_price'        => 246.0,
            'sku'                => 'TT102',
            'product_attributes' => [],
            'item_meta'          => [],
        ];
    }


    public function testShouldCreateFilledItem() {
        $item = ( new FeeItem() )
            ->set_name( 'PayU' )
            ->set_unit( 'item' )
            ->set_qty( 2 )
            ->set_net_price( 100.00 )
            ->set_discount( 0.0 )
            ->set_net_price_sum( 200.00 )
            ->set_vat_rate( 23 )
            ->set_vat_sum( 46.00 )
            ->set_vat_type_index( 0 )
            ->set_gross_price( 246.00 )
            ->set_sku( 'TT102' )
            ->get();
        $this->assertSame( $item, $this->filledItemData() );
    }

}
