<?php

namespace Tests\Invoices\Items;

use PHPUnit\Framework\TestCase;
use WPDesk\Library\FlexibleInvoicesCore\Documents\Items\DiscountItem;
use WPDesk\Library\FlexibleInvoicesCore\Documents\Items\FeeItem;
use WPDesk\Library\FlexibleInvoicesCore\Documents\Items\ProductItem;
use WPDesk\Library\FlexibleInvoicesCore\Documents\Items\ShippingItem;
use WPDesk\Library\FlexibleInvoicesCore\Documents\Items\WooProductItem;
use WPDesk\Library\FlexibleInvoicesCore\Documents\Items\ItemFactory;

class ItemFactoryTest extends TestCase {

    public function testShouldGetProductItem() {
        $factory = new ItemFactory( '' );
        $this->assertInstanceOf( ProductItem::class, $factory->get_item() );
    }

    public function testShouldGetWooProductItem() {
        $factory = new ItemFactory( 'line_item' );
        $this->assertInstanceOf( WooProductItem::class, $factory->get_item() );
    }

    public function testShouldGetShippingItem() {
        $factory = new ItemFactory( 'shipping' );
        $this->assertInstanceOf( ShippingItem::class, $factory->get_item() );
    }

    public function testShouldGetFeeItem() {
        $factory = new ItemFactory( 'fee' );
        $this->assertInstanceOf( FeeItem::class, $factory->get_item() );
    }

    public function testShouldGetCouponItem() {
        $factory = new ItemFactory( 'discount' );
        $this->assertInstanceOf( DiscountItem::class, $factory->get_item() );
    }

}
