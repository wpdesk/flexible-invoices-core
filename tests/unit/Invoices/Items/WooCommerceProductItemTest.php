<?php

namespace Tests\Invoices\Items;

use PHPUnit\Framework\TestCase;
use WPDesk\Library\FlexibleInvoicesCore\Documents\Items\WooProductItem;

class WooCommerceProductItemTest extends TestCase {

    /**
     * @return array
     */
    private function emptyItemData(): array {
        return [
            'type'               => 'product',
            'name'               => 'DHL',
            'unit'               => 'item',
            'quantity'           => 1.0,
            'net_price'          => 0.0,
            'discount'           => 0.0,
            'net_price_sum'      => 0.0,
            'vat_rate'           => 0.0,
            'vat_sum'            => 0.0,
            'vat_type'           => 0,
            'vat_type_name'      => '0%',
            'vat_type_index'     => 0,
            'total_price'        => 0.0,
            'sku'                => '',
            'product_attributes' => [],
            'item_meta'          => [],
            'wc_item_type'       => 'line_item',
            'wc_order_item_id'   => 0,
            'wc_product_id'      => 0,
            'wc_variation_id'    => 0,
        ];
    }

    public function testShouldCreateEmptyItem() {
        $item = ( new WooProductItem() )->set_name( 'DHL' )->get();
        $this->assertEquals( $item, $this->emptyItemData() );
    }

    /**
     * @return array
     */
    private function filledItemData(): array {
        return [
            'type'               => 'product',
            'name'               => 'T-shirt Red',
            'unit'               => 'item',
            'quantity'           => 2.0,
            'net_price'          => 100.0,
            'discount'           => 0.0,
            'net_price_sum'      => 200.0,
            'vat_rate'           => 22.5,
            'vat_sum'            => 46.00,
            'vat_type'           => 22.5,
            'vat_type_name'      => '22.5%',
            'vat_type_index'     => 0,
            'total_price'        => 246.0,
            'sku'                => 'TT102',
            'product_attributes' => [],
            'item_meta'          => [],
            'wc_item_type'       => 'line_item',
            'wc_order_item_id'   => 100,
            'wc_product_id'      => 10,
            'wc_variation_id'    => 12,
        ];
    }


    public function testShouldCreateFilledItem() {
        $item = ( new WooProductItem() )
            ->set_name( 'T-shirt Red' )
            ->set_unit( 'item' )
            ->set_qty( 2 )
            ->set_net_price( 100.00 )
            ->set_discount( 0.0 )
            ->set_net_price_sum( 200.00 )
            ->set_vat_rate( 22.5 )
            ->set_vat_sum( 46.00 )
            ->set_vat_type_index( 0 )
            ->set_gross_price( 246.00 )
            ->set_sku( 'TT102' )
            ->set_wc_item_type( 'line_item' )
            ->set_wc_order_item_id( 100 )
            ->set_wc_product_id( 10 )
            ->set_wc_variation_id( 12 )
            ->get();
        $this->assertSame( $item, $this->filledItemData() );
    }

}
